<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLogEntriesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_monitor_log_entries',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('type')->unsigned();
                $table->integer('server_id')->nullable()->index();
                $table->integer('website_id')->nullable()->index();
                $table->integer('process_id')->nullable()->index();
                $table->boolean('was_online')->default(false);
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_log_entries');
    }
}
