<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateOwnersPivot extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_monitor_user_processes',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('user_id')->unsigned();
                $table->integer('process_id')->unsigned();
                $table->primary(['user_id', 'process_id']);
            }
        );
        Schema::create(
            'keios_monitor_user_servers',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('user_id')->unsigned();
                $table->integer('server_id')->unsigned();
                $table->primary(['user_id', 'server_id']);
            }
        );
        Schema::create(
            'keios_monitor_user_websites',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('user_id')->unsigned();
                $table->integer('website_id')->unsigned();
                $table->primary(['user_id', 'website_id']);
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_user_processes');
        Schema::dropIfExists('keios_monitor_user_servers');
        Schema::dropIfExists('keios_monitor_user_websites');
    }
}
