<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProcessesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_monitor_processes', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('server_id')->index();
            $table->string('process_name')->index();
            $table->string('process_port')->nullable();
            $table->string('check_using');
            $table->boolean('is_enabled')->default(false);
            $table->boolean('is_online')->default(false);
            $table->boolean('send_email')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_processes');
    }
}
