<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServersTable extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_monitor_servers',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('host')->index();
                $table->integer('ssh_port');
                $table->string('ssh_user');
                $table->boolean('is_enabled')->default(false);
                $table->boolean('is_online')->default(false);
                $table->boolean('send_email')->default(false);
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_servers');
    }
}
