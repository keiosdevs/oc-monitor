<?php namespace Keios\Monitor\Updates;

use Keios\Monitor\Models\MonitorProfile;
use RainLab\User\Models\User;
use Schema;
use October\Rain\Database\Updates\Migration;


class CreateProfilesForExistingUsers extends Migration
{
    public function up()
    {
        $users = User::all();
        foreach($users as $user){
            if(!$user->monitorprofile){
                $id = $user->id;
                $profile = new MonitorProfile();
                $profile->user_id = $id;
                $profile->processes_limit = 10;
                $profile->servers_limit = 10;
                $profile->websites_limit = 10;
                $profile->manage_processes = true;
                $profile->manage_servers = true;
                $profile->manage_websites = true;
                $profile->save();
            }
        }
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_log_entries');
    }
}
