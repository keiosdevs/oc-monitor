<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateMonitorProfilesTable
 *
 * @package Keios\Monitor\Updates
 */
class CreateMonitorProfilesTable extends Migration
{
    public function up()
    {
        Schema::create('keios_monitor_monitor_profiles', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('processes_limit')->default(10);
            $table->integer('servers_limit')->default(10);
            $table->integer('websites_limit')->default(10);
            $table->boolean('manage_processes')->default(false);
            $table->boolean('manage_servers')->default(false);
            $table->boolean('manage_websites')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_monitor_profiles');
    }
}
