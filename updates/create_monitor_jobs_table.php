<?php namespace Keios\Monitor\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMonitorJobsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'keios_monitor_jobs',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('status')->nullable();
                $table->string('type')->nullable();
                $table->integer('user_id');
                $table->integer('progress_current')->nullable();
                $table->integer('progress_max')->nullable();
                $table->string('metadata')->nullable();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('keios_monitor_servers');
    }
}
