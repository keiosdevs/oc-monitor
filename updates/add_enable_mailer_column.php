<?php namespace Keios\Monitor\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddEnableMailerColumn
 *
 * @package Keios\Monitor\Updates
 */
class AddEnableMailerColumn extends Migration
{
    /**
     *
     */
    public function up()
    {
        Schema::table(
            'keios_monitor_monitor_profiles',
            function (Blueprint $table) {
                $table->addColumn('boolean', 'enable_mailer',['after' => 'manage_websites', 'default' => false]);
            }
        );
    }

    /**
     *
     */
    public function down()
    {
        Schema::table(
            'keios_monitor_monitor_profiles',
            function ($table) {
                $table->dropColumn('enable_mailer');
            }
        );
    }
}
