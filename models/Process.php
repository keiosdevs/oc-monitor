<?php namespace Keios\Monitor\Models;

use Keios\Monitor\Classes\CacheInvalidator;
use Model;

/**
 * Process Model
 */
class Process extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_monitor_processes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $hasMany = [
        'logs' => ['Keios\Monitor\Models\LogEntry']
    ];
    /**
     * @var array Relations
     */
    public $belongsTo = [
        'server' => 'Keios\Monitor\Models\Server',
    ];

    /**
     * @var array
     */
    public $belongsToMany = [
        'users' => ['Rainlab\User\Models\User', 'table' => 'keios_monitor_user_processes', 'otherKey' => 'user_id', 'key' => 'process_id'],
    ];

    /**
     * Clear cache
     */
    public function afterSave()
    {
        $this->clearCache();
    }

    /**
     * Clear cache
     */
    public function afterCreate()
    {
        $this->clearCache();
    }

    /**
     * Cache clear
     */
    public function afterDelete()
    {
        $this->clearCache();
    }

    /**
     * Cache clear
     */
    public function clearCache(){
        $user = \Auth::getUser();
        $cacheInvalidator = new CacheInvalidator();
        $cacheInvalidator->clearProcessCache($this->id, $user);
    }
}