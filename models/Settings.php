<?php

namespace Keios\Monitor\Models;

use October\Rain\Database\Model;


/**
 * Class Settings
 *
 * @package Keios\GeoLocaleSwitcher
 */
class Settings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'keios_monitor_settings';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yml';

}