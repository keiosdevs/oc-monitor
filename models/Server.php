<?php namespace Keios\Monitor\Models;

use Keios\Monitor\Classes\CacheInvalidator;
use Model;

/**
 * Server Model
 */
class Server extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_monitor_servers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    public $hasMany = [
        'logs' => ['Keios\Monitor\Models\LogEntry'],
    ];

    /**
     * @var array
     */
    public $belongsToMany = [
        'users' => [
            'Rainlab\User\Models\User',
            'table' => 'keios_monitor_user_servers',
            'key' => 'server_id',
            'otherKey' => 'user_id',
        ],
    ];


    /**
     * Clear cache
     */
    public function afterSave()
    {
        $this->clearCache();
    }

    /**
     * Clear cache
     */
    public function afterCreate()
    {
        $this->clearCache();
    }

    /**
     * Cache clear
     */
    public function afterDelete()
    {
        $this->clearCache();
    }

    /**
     * Cache clear
     */
    public function clearCache()
    {
        $user = \Auth::getUser();
        $cacheInvalidator = new CacheInvalidator();
        $cacheInvalidator->clearServersCache($this->id, $user);
    }
}