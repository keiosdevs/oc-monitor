<?php namespace Keios\Monitor\Models;

use Model;

/**
 * MonitorProfile Model
 */
class MonitorProfile extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_monitor_monitor_profiles';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'user' => 'RainLab\User\Models\User',
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
