<?php namespace Keios\Monitor\Models;

use Model;

/**
 * LogEntry Model
 */
class LogEntry extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_monitor_log_entries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'website' => 'Keios\Monitor\Models\Website',
        'process' => 'Keios\Monitor\Models\Process',
        'server'  => 'Keios\Monitor\Models\Server',
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}