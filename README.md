## oc-monitor

KeiosMonitor is a plugin for OctoberCMS, allowing simple monitoring of SSH hosts, processes and websites.

![pic](https://i.keios.eu/jz/issue-161208-120720.png)

![pic2](https://i.keios.eu/jz/issue-161208-121119.png)

![pic3](https://i.keios.eu/jz/issue-161208-120910.png)

### Installation

Install plugin:

```
$ cd $OCTOBER_INSTANCE/plugins
$ mkdir keios && cd keios
$ git clone git@bitbucket.org:keiosdevs/oc-monitor.git monitor
$ cd monitor && composer update
$ cd $OCTOBER_INSTANCE
$ php artisan october:up
```

Optionally, install theme:

```
$ cd $OCTOBER_INSTANCE/themes
$ git clone git@bitbucket.org:keiosdevs/oc-monitor-theme.git monitor
```

chown/chmod it and enable it in backend

### Configuration

1. **Configure queues! Use redis, it's easy to configure. Seriously, it will make app 5 times faster.**
2. Go to OctoberCMS Backend -> Settings -> Keios Monitor
3. Enter cron-format schedule string in first three fields (eg */5 * * * * to check status every five minutes)
4. Enter path to SSH key that should be used to authorize on monitored servers
5. Enter e-mail, to which warnings should be sent.
6. Add cronjob for October scheduler to run every minute. If your october is in /var/www/monitor, it should look as follows:

```
$ sudo crontab -e

* * * * * /usr/bin/php /var/www/monitor/artisan schedule:run
```

### Apparatus

We advise to boost app with <a href="https://github.com/keiosweb/oc-apparatus-lib"><b>Apparatus</b></a> for notifications.


### Component

Plugin comes with a km_livemonitor component (you may want to install [oc-monitor-theme](https://bitbucket.org/keiosdevs/oc-monitor-theme) for it).

User plugin support is planned, so when using it please make sure that only you have access to Monitor October Instance.

![component](https://i.keios.eu/jz/mon-161209-074116.png)

Component will display data according to its properties. It refreshes data every 30 seconds and supports browser notifications.

### Usage

#### Server Monitoring

1. Go to OctoberCMS Backend -> Monitor and add a host.
2. Authorize your SSH on that host, by adding your id_rsa.pub key to selected user's ~/.ssh/authorized_keys

You can test if all is working by running

```
php artisan monitor:check-server
```

#### Process Monitoring

Process belong to servers and are checked on the server you select while creating them.

You can use two modes of checking if process is running: **ps** and **netstat**.

First one will run

```
ps aux|grep -v "grep"|grep -c $processName
```

on the server, second one will run

```
netstat -anp|grep $processName|grep -c $processPort
```

You can check if everything is working with

```
php artisan monitor:check-process
```

#### Website monitoring

Website monitoring is done with curl and will report website offline if CURL gets any response different than 200, 201, 301, 302.

Remember that monitor may do false traffic in your Analytics and you should black list the monitoring server IP.

You can check if everything is working with

```
php artisan monitor:check-website
```