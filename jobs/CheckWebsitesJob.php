<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/15/16
 * Time: 4:25 PM
 */

namespace Keios\Monitor\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\InteractsWithQueue;
use Keios\Monitor\Classes\CheckWebsite;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Contracts\JobIdAssignable;


/**
 * Class CheckWebsitesJob
 *
 * @package Keios\Monitor\Jobs
 */
class CheckWebsitesJob implements SelfHandling, JobIdAssignable
{
    use InteractsWithQueue;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var
     */
    private $userId;

    /**
     * @var
     */
    private $jobId;

    /**
     * @var CheckWebsite
     */
    private $checkWebsite;

    /**
     * @var WebsiteRepository
     */
    private $websiteRepo;

    /**
     * @param $userId
     *
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->checkWebsite = new CheckWebsite();
        $this->websiteRepo = new WebsiteRepository();
    }

    /**
     *
     * @param JobManager $jobManager
     *
     * @throws \Exception
     */
    public function handle(
        JobManager $jobManager
    ) {
        $this->jobManager = $jobManager;
        $this->jobManager->startJob($this->jobId, 1);
        try {
            $this->checkWebsite->check();
            $this->jobManager->updateJobState($this->jobId, 1);
        } catch (\Exception $e) {
            \Log::error($e->getMessage().$e->getTraceAsString());
            $this->jobManager->failJob($this->jobId, ['error' => $e->getMessage()]);
        }

        $this->jobManager->completeJob($this->jobId);
    }

    /**
     * @param int $id
     */
    public function assignJobId($id)
    {
        $this->jobId = $id;
    }
}