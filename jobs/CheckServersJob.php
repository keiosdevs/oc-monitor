<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/15/16
 * Time: 4:25 PM
 */

namespace Keios\Monitor\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\InteractsWithQueue;
use Keios\Monitor\Classes\CheckServer;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Classes\ServerRepository;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Contracts\JobIdAssignable;


/**
 * Class CheckWebsitesJob
 *
 * @package Keios\Monitor\Jobs
 */
class CheckServersJob implements SelfHandling, JobIdAssignable
{
    use InteractsWithQueue;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var
     */
    private $userId;

    /**
     * @var
     */
    private $jobId;

    /**
     * @var CheckServer
     */
    private $checkServer;

    /**
     * @var WebsiteRepository
     */
    private $serversRepo;

    /**
     * RatesCopyJob constructor.
     *
     * Takes source tariff ID and empty, freshly created target tariff ID.
     *
     * @param $userId
     *
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->checkServer = new CheckServer();
        $this->serversRepo = new ServerRepository();
    }

    /**
     * We pass application settings model here, VoipSwitch Tools library Manager
     * instance and JobManager instance.
     *
     * @param JobManager $jobManager
     *
     * @throws \Exception
     */
    public function handle(
        JobManager $jobManager
    ) {
        $this->jobManager = $jobManager;


        $servers = $this->serversRepo->getAllUserServers($this->userId);
        $this->jobManager->startJob($this->jobId, count($servers));
        $indexLoop = 0;
        foreach ($servers as $server) {
            try {
                $this->checkServer->check($server, $this->userId);
                $this->jobManager->updateJobState($this->jobId, $indexLoop);
                ++$indexLoop;
            } catch (\Exception $e) {
                \Log::error($e->getMessage().$e->getTraceAsString());
                $this->jobManager->failJob($this->jobId, ['error' => $e->getMessage()]);
            }
        }
        $this->jobManager->completeJob($this->jobId);
    }

    /**
     * @param int $id
     */
    public function assignJobId($id)
    {
        $this->jobId = $id;
    }
}