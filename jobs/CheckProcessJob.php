<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/15/16
 * Time: 4:25 PM
 */

namespace Keios\Monitor\Jobs;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\InteractsWithQueue;
use Keios\Monitor\Classes\CheckProcess;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Classes\ProcessRepository;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Contracts\JobIdAssignable;
use Keios\Monitor\Models\Process;


/**
 * Class CheckWebsitesJob
 *
 * @package Keios\Monitor\Jobs
 */
class CheckProcessJob implements SelfHandling,JobIdAssignable
{
    use InteractsWithQueue;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var
     */
    private $userId;

    /**
     * @var
     */
    private $jobId;

    /**
     * @var CheckProcess
     */
    private $checkProcess;

    /**
     * @var WebsiteRepository
     */
    private $processRepo;

    /**
     * RatesCopyJob constructor.
     *
     * Takes source tariff ID and empty, freshly created target tariff ID.
     *
     * @param int $userId
     *
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->checkProcess = new CheckProcess();
        $this->processRepo = new ProcessRepository();
    }

    /**
     * We pass application settings model here, VoipSwitch Tools library Manager
     * instance and JobManager instance.
     *
     * @param JobManager $jobManager
     *
     * @throws \Exception
     */
    public function handle(
        JobManager $jobManager
    ) {
        $this->jobManager = $jobManager;


        /** @var Process[] $processes */
        $processes = $this->processRepo->getAllUserProcesses($this->userId);
        $this->jobManager->startJob($this->jobId, count($processes));
        $indexLoop = 0;
        foreach ($processes as $process) {
            try {
                $this->checkProcess->check($process, $this->userId);
                $this->jobManager->updateJobState($this->jobId, $indexLoop);
                ++$indexLoop;
            } catch(\Exception $e){
                \Log::error($e->getMessage().$e->getTraceAsString());
                $this->jobManager->failJob($this->jobId, ['error' => $e->getMessage()]);
            }
        }
        $this->jobManager->completeJob($this->jobId);
    }

    /**
     * @param int $id
     */
    public function assignJobId($id)
    {
        $this->jobId = $id;
    }
}