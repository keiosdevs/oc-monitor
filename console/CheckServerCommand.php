<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/30/16
 * Time: 8:38 PM
 */

namespace Keios\Monitor\Console;

use Illuminate\Console\Command;
use Keios\Monitor\Classes\CheckServer;
use Keios\Monitor\Classes\Logger;
use Keios\Monitor\Classes\SshRunner;
use Keios\Monitor\Models\Server;
use Symfony\Component\Console\Input\InputArgument;
use SSH;

/**
 * Class KickCommand
 *
 * @package Pixelpixel\Mgmxsimulator\Console
 */
class CheckServerCommand extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'monitor:check-server';

    /**
     * The console command description.
     */
    protected $description = 'Check server status';


    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var CheckServer
     */
    protected $checkServer;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CheckWebsiteCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->sshRunner = new SshRunner();
        $this->checkServer = new CheckServer();
        $this->logger = new Logger();
    }

    /**
     * Execute the console command.
     *
     * @throws \ApplicationException
     * @throws \InvalidArgumentException
     */
    public function fire()
    {
        /**
         * @var array $successes
         * @var array $errors
         **/
        $host = $this->argument('host');
        $server = null;
        if ($host) {
            $server = Server::where('host', $host)->first();
        }
        $result = $this->checkServer->check($server);
        $errors = $result['errors'];
        $successes = $result['success'];
        foreach($successes as $success){
            $this->info($success);
        }
        foreach($errors as $error){
            $this->error($error);
        }
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'host',
                InputArgument::OPTIONAL,
                'ssh host',
            ],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}