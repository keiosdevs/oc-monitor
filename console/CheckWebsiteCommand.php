<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/30/16
 * Time: 8:38 PM
 */

namespace Keios\Monitor\Console;

use Illuminate\Console\Command;
use Keios\Monitor\Classes\CheckWebsite;
use Keios\Monitor\Classes\Logger;
use Keios\Monitor\Classes\SshRunner;
use Keios\Monitor\Models\Website;
use Symfony\Component\Console\Input\InputArgument;
use SSH;

/**
 * Class KickCommand
 *
 * @package Pixelpixel\Mgmxsimulator\Console
 */
class CheckWebsiteCommand extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'monitor:check-website';

    /**
     * The console command description.
     */
    protected $description = 'Check website status';


    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var CheckWebsite
     */
    protected $checkWebsite;

    /**
     * @var Logger
     */
    protected $logger;
    /**
     * CheckWebsiteCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->sshRunner = new SshRunner();
        $this->checkWebsite = new CheckWebsite();
        $this->logger = new Logger();
    }

    /**
     * Execute the console command.
     *
     * @throws \ApplicationException
     */
    public function fire()
    {
        /**
         * @var array $successes
         * @var array $errors
         **/
        $sWebsite = $this->argument('url');
        $website = null;
        if ($sWebsite) {
            $website = Website::where('url', $sWebsite)->first();
        }
        $result = $this->checkWebsite->check($website);
        $errors = $result['errors'];
        $successes = $result['success'];
        foreach($successes as $success){
            $this->info($success);
        }
        foreach($errors as $error){
            $this->error($error);
        }
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'url',
                InputArgument::OPTIONAL,
                'website url',
            ],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}