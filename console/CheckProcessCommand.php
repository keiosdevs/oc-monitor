<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/30/16
 * Time: 8:38 PM
 */

namespace Keios\Monitor\Console;

use Illuminate\Console\Command;
use Keios\Monitor\Classes\CheckProcess;
use Keios\Monitor\Classes\Logger;
use Keios\Monitor\Classes\SshRunner;
use Keios\Monitor\Models\Process;
use Symfony\Component\Console\Input\InputArgument;
use SSH;

/**
 * Class KickCommand
 *
 * @package Pixelpixel\Mgmxsimulator\Console
 */
class CheckProcessCommand extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'monitor:check-process';

    /**
     * The console command description.
     */
    protected $description = 'Check process status';


    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var CheckProcess
     */
    protected $checkProcess;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CheckWebsiteCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->sshRunner = new SshRunner();
        $this->checkProcess = new CheckProcess();
        $this->logger = new Logger();
    }

    /**
     * Execute the console command.
     *
     * @throws \ApplicationException
     * @throws \InvalidArgumentException
     */
    public function fire()
    {
        /**
         * @var array $successes
         * @var array $errors
         **/
        $sProcess = $this->argument('process');
        $process = null;
        if ($sProcess) {
            $process = Process::where('process_name', $sProcess)->first();
        }
        $result = $this->checkProcess->check($process);
        
        $errors = $result['errors'];
        $successes = $result['success'];
        foreach ($successes as $success) {
            $this->info($success);
        }
        foreach ($errors as $error) {
            $this->error($error);
        }
    }


    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'process',
                InputArgument::OPTIONAL,
                'process to check',
            ],
        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}