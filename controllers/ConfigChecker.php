<?php namespace Keios\Monitor\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Keios\Monitor\Models\Settings;
use Lang;

/**
 * Config Checker Back-end Controller
 */
class ConfigChecker extends Controller
{


    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = 'Check Monitor Configuration';
        BackendMenu::setContext('Keios.Monitor', 'monitor', 'configchecker');
    }

    public function index()
    {
        $checked = $this->check();
        $this->vars['queue_status'] = $checked['queue'];
        $this->vars['cache_status'] = $checked['cache'];
        $this->vars['config_status'] = $checked['config'];
        $this->vars['deps_status'] = $checked['deps'];

    }

    public function onInstallDrivers()
    {
        \Artisan::call('plugin:install', ['name' => 'october.drivers']);
    }

    public function onCheck()
    {

        $checked = $this->check();
        $this->vars['queue_status'] = $checked['queue'];
        $this->vars['cache_status'] = $checked['cache'];
        $this->vars['config_status'] = $checked['config'];
        $this->vars['deps_status'] = $checked['deps'];
    }

    public function check()
    {
        $settingsUrl = \Backend::url().'/system/settings/update/keios/monitor/config';
        $cacheAdvises = [
            'is_file'    => 'You are using file cache. We recommend to change it to <i>redis</i> or <i>memcached</i>',
            'def_prefix' => 'You are using default cache prefix. Change it from <i>october</i> to unique key.',
        ];
        $queueAdvises = [
            'is_sync' => 'You are using sync queue driver. That means queue jobs are not processed async. <a href="https://octobercms.com/docs/services/queues" target="_blank">More details</a>',
        ];
        $configAdvises = [
            'no_website_queue' => 'You have no cron definition for website check task. Please enter for example */5 * * * * in <a href="'.$settingsUrl.'">Monitor Settings</a>.',
            'no_server_queue'  => 'You have no cron definition for server check task. Please enter for example */5 * * * * in <a href="'.$settingsUrl.'">Monitor Settings</a>.',
            'no_process_queue' => 'You have no cron definition for process check task. Please enter for example */5 * * * * in <a href="'.$settingsUrl.'">Monitor Settings</a>.',
            'no_ssh_key'       => 'You have no SSH key path set. You can only monitor websites with current configuration',
            'no_logging'       => 'You have logging disabled. Log and Charts modules will not work',
            'no_mail'          => 'No mail configured for monitoring. Will not send e-mails.',
        ];

        $dependencyAdvises = [
            'no_drivers'   => 'You do not have October Drivers installed. <a data-request="onInstallDrivers" href="#" data-stripe-load-indicator data-request-confirm="This will install october.drivers plugin. Are you sure?">Install it with one click</a> or <a href="https://octobercms.com/plugin/october-drivers" target="_blank">add it to project</a>',
            'no_apparatus' => 'You do not have Apparatus installed. You will not have notifications. <a href="https://bitbucket.org/keiosdevs/oc-apparatus" target="_blank">Find it here</a>',
        ];

        $cacheConfig = \Config::get('cache');
        $queueConfig = \Config::get('queue');
        $settings = Settings::instance();
        $servers_check_delay = $settings->get('servers_check_delay');
        $websites_check_delay = $settings->get('websites_check_delay');
        $processes_check_delay = $settings->get('processes_check_delay');
        $ssh_key_path = $settings->get('ssh_key_path');
        $email = $settings->get('email');
        $logging = $settings->get('enable_logging');


        $cache = [
            'engine'  => $cacheConfig['default'],
            'status'  => '',
            'message' => [],
        ];
        $queue = [
            'engine'  => $queueConfig['default'],
            'status'  => '',
            'message' => [],
        ];
        $configuration = [
            'engine'  => '',
            'status'  => '',
            'message' => [],
        ];
        $dependencies = [
            'engine'  => '',
            'status'  => 'ok',
            'message' => [],
        ];


        if ($cacheConfig['default'] === 'file') {
            $cache['status'] = 'warn';
            $cache['message'][] = $cacheAdvises['is_file'];
        }
        if ($cacheConfig['prefix'] === 'october') {
            $cache['status'] = 'warn';
            $cache['message'][] = $cacheAdvises['def_prefix'];
        }
        if ($queueConfig['default'] === 'sync') {
            $queue['status'] = 'warn';
            $queue['message'][] = $queueAdvises['is_sync'];
        }
        if (!$email) {
            $configuration['status'] = 'ok';
            $configuration['message'][] = $configAdvises['no_mail'];
        }
        if (!$logging) {
            $configuration['status'] = 'warn';
            $configuration['message'][] = $configAdvises['no_logging'];
        }
        if (!$ssh_key_path) {
            $configuration['status'] = 'warn';
            $configuration['message'][] = $configAdvises['no_ssh_key'];
        }
        if (!$servers_check_delay) {
            $configuration['status'] = 'error';
            $configuration['message'][] = $configAdvises['no_server_queue'];
        }
        if (!$websites_check_delay) {
            $configuration['status'] = 'error';
            $configuration['message'][] = $configAdvises['no_website_queue'];
        }
        if (!$processes_check_delay) {
            $configuration['status'] = 'error';
            $configuration['message'][] = $configAdvises['no_process_queue'];
        }
        if(!class_exists('Keios\Apparatus\Plugin')) {
            $dependencies['status'] = 'warn';
            $dependencies['message'][] = $dependencyAdvises['no_apparatus'];
        }
        if (!class_exists('Predis\Client')) {
            $dependencies['status'] = 'error';
            $dependencies['message'][] = $dependencyAdvises['no_drivers'];
        }



        return ['cache' => $cache, 'queue' => $queue, 'config' => $configuration, 'deps' => $dependencies];
    }
}
