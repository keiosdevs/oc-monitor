<?php namespace Keios\Monitor\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Monitor\Models\Process;

/**
 * Processes Back-end Controller
 */
class Processes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Monitor', 'monitor', 'processes');
    }

    /**
     * Deleted checked processes.
     */
    public function index_onDelete()
    {
        /** @var array $checkedIds */
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $processId) {
                if (!$process = Process::find($processId)) {
                    continue;
                }
                $process->delete();
            }

            Flash::success(Lang::get('keios.monitor::lang.processes.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.monitor::lang.processes.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
