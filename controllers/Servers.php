<?php namespace Keios\Monitor\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Monitor\Models\Server;

/**
 * Servers Back-end Controller
 */
class Servers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Monitor', 'monitor', 'servers');
    }

    /**
     * Deleted checked servers.
     */
    public function index_onDelete()
    {
        /** @var array $checkedIds */
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $serverId) {
                if (!$server = Server::find($serverId)) {
                    continue;
                }
                $server->delete();
            }

            Flash::success(Lang::get('keios.monitor::lang.servers.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.monitor::lang.servers.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
