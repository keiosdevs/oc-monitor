<?php namespace Keios\Monitor\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Monitor\Models\Website;

/**
 * Websites Back-end Controller
 */
class Websites extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Monitor', 'monitor', 'websites');
    }

    /**
     * Deleted checked websites.
     */
    public function index_onDelete()
    {
        /** @var array $checkedIds */
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $websiteId) {
                if (!$website = Website::find($websiteId)) {
                    continue;
                }
                $website->delete();
            }

            Flash::success(Lang::get('keios.monitor::lang.websites.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.monitor::lang.websites.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
