<?php namespace Keios\Monitor\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Keios\Monitor\Models\LogEntry;

/**
 * Log Entries Back-end Controller
 */
class LogEntries extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Monitor', 'monitor', 'logentries');
    }

    /**
     * Deleted checked logentries.
     */
    public function index_onDelete()
    {
        $data = post();
        if (($checkedIds = $data['checked']) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $logentryId) {
                if (!$logentry = LogEntry::find($logentryId)) {
                    continue;
                }
                $logentry->delete();
            }

            Flash::success(Lang::get('keios.monitor::lang.logentries.delete_selected_success'));
        } else {
            Flash::error(Lang::get('keios.monitor::lang.logentries.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
