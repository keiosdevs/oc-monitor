<?php namespace Keios\Monitor;

use Backend;
use Backend\Widgets\Form;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Classes\SshServiceProvider;
use Keios\Monitor\Components\HelperComponent;
use Keios\Monitor\Components\LogWidget;
use Keios\Monitor\Components\ManageServices;
use Keios\Monitor\Components\AuthDataComponent;
use Keios\Monitor\Components\Charts;
use Keios\Monitor\Components\LiveMonitor;
use Keios\Monitor\Models\MonitorProfile;
use Keios\Monitor\Models\Settings;
use RainLab\User\Controllers\Users;
use RainLab\User\Models\User;
use System\Classes\PluginBase;

/**
 * Monitor Plugin Information File
 */
class Plugin extends PluginBase
{


    public $require = [
        'RainLab.User',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.plugin.name',
            'description' => 'keios.monitor::lang.plugin.description',
            'author'      => 'Keios',
            'icon'        => 'icon-server',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

        \App::register(SshServiceProvider::class);
        $this->app->bind(
            'monitor:check-server',
            'Keios\Monitor\Console\CheckServerCommand'
        );
        $this->app->bind(
            'monitor:check-website',
            'Keios\Monitor\Console\CheckWebsiteCommand'
        );
        $this->app->bind(
            'monitor:check-process',
            'Keios\Monitor\Console\CheckProcessCommand'
        );
        $this->app->bind(
            JobManager::class,
            function () {
                return new JobManager(
                    $this->app->make('db')->connection(), $this->app->make('queue')->connection()
                );
            }
        );

        $this->commands(
            [
                'monitor:check-server',
                'monitor:check-website',
                'monitor:check-process',
            ]
        );
    }


    protected function extendUser()
    {
        User::extend(function($model) {
            $model->hasOne['monitorprofile'] = ['Keios\Monitor\Models\MonitorProfile', 'key' => 'user_id'];

        });
        User::extend(function($model) {
            $model->bindEvent('model.afterCreate', function() use ($model) {
                $id = $model->id;
                $profile = new MonitorProfile();
                $profile->user_id = $id;
                $profile->processes_limit = 10;
                $profile->servers_limit = 10;
                $profile->websites_limit = 10;
                $profile->manage_processes = true;
                $profile->manage_servers = true;
                $profile->manage_websites = true;
                $profile->save();
            });
        });
        $this->app['events']->listen(
            'backend.form.extendFields',
            function (Form $form) {

                if (!$form->model instanceof User) {
                    return;
                }


                $tabFields = [];
                $secondaryTabFields['monitorprofile[enable_mailer]@update'] = [
                    'label' => 'keios.monitor::lang.app.enable_mailer',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_processes]@update'] = [
                    'label' => 'keios.monitor::lang.app.manage_processes',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_websites]@update'] = [
                    'label' => 'keios.monitor::lang.app.manage_websites',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_servers]@update'] = [
                    'label' => 'keios.monitor::lang.app.manage_servers',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[processes_limit]@update'] = [
                    'label' => 'keios.monitor::lang.app.processes_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];
                $secondaryTabFields['monitorprofile[servers_limit]@update'] = [
                    'label' => 'keios.monitor::lang.app.servers_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];
                $secondaryTabFields['monitorprofile[websites_limit]@update'] = [
                    'label' => 'keios.monitor::lang.app.websites_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];


                $secondaryTabFields['monitorprofile[enable_mailer]@preview'] = [
                    'label' => 'keios.monitor::lang.app.enable_mailer',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_processes]@preview'] = [
                    'label' => 'keios.monitor::lang.app.manage_processes',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_websites]@preview'] = [
                    'label' => 'keios.monitor::lang.app.manage_websites',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[manage_servers]@preview'] = [
                    'label' => 'keios.monitor::lang.app.manage_servers',
                    'type'  => 'switch',
                ];
                $secondaryTabFields['monitorprofile[processes_limit]@preview'] = [
                    'label' => 'keios.monitor::lang.app.processes_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];
                $secondaryTabFields['monitorprofile[servers_limit]@preview'] = [
                    'label' => 'keios.monitor::lang.app.servers_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];
                $secondaryTabFields['monitorprofile[websites_limit]@preview'] = [
                    'label' => 'keios.monitor::lang.app.websites_limit',
                    'placeholder' => 10,
                    'type'  => 'number',
                ];

                $form->addTabFields($tabFields);
                $form->addSecondaryTabFields($secondaryTabFields);
            }
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $this->extendUser();
        $connections = $this->app->make('monitor.connections');
        $connections->configureAll();

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            LiveMonitor::class       => 'km_livemonitor',
            Charts::class            => 'km_charts',
            AuthDataComponent::class => 'km_authdata',
            ManageServices::class    => 'km_manageservices',
            HelperComponent::class   => 'km_helper',
            LogWidget::class         => 'km_logwidget',
        ];
    }

    /**
     * @param string $schedule
     */
    public function registerSchedule($schedule)
    {
        $settings = Settings::instance();
        $serversSchedule = $settings->get('servers_check_delay');
        $websiteSchedule = $settings->get('websites_check_delay');
        $processesSchedule = $settings->get('processes_check_delay');
        $schedule->command('monitor:check-server')->cron($serversSchedule);
        $schedule->command('monitor:check-process')->cron($processesSchedule);
        $schedule->command('monitor:check-website')->cron($websiteSchedule);
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'keios.monitor.servers'   => [
                'tab'   => 'keios.monitor::lang.plugin.name',
                'label' => 'keios.monitor::lang.permissions.servers',
            ],
            'keios.monitor.websites'  => [
                'tab'   => 'keios.monitor::lang.plugin.name',
                'label' => 'keios.monitor::lang.permissions.websites',
            ],
            'keios.monitor.processes' => [
                'tab'   => 'keios.monitor::lang.plugin.name',
                'label' => 'keios.monitor::lang.permissions.processes',
            ],
            'keios.monitor.settings'  => [
                'tab'   => 'keios.monitor::lang.plugin.name',
                'label' => 'keios.monitor::lang.permissions.settings',
            ],
            'keios.monitor.check_config'  => [
                'tab'   => 'keios.monitor::lang.plugin.name',
                'label' => 'keios.monitor::lang.permissions.check_config',
            ],
        ];
    }


    /**
     * @return array
     */
    public function registerMailTemplates()
    {
        return [
            'keios.monitor::mail.server'  => 'Sent on server status change.',
            'keios.monitor::mail.website' => 'Sent on website status change.',
            'keios.monitor::mail.process' => 'Sent on process status change.',
        ];
    }


    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'monitor' => [
                'label'       => 'Monitor',
                'url'         => Backend::url('keios/monitor/servers'),
                'icon'        => 'icon-server',
                'permissions' => ['keios.monitor.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'servers'   => [
                        'label'       => 'keios.monitor::lang.app.servers',
                        'icon'        => 'icon-server',
                        'url'         => Backend::url('keios/monitor/servers'),
                        'permissions' => ['keios.monitor.servers'],
                    ],
                    'websites'  => [
                        'label'       => 'keios.monitor::lang.app.websites',
                        'icon'        => 'icon-bookmark',
                        'url'         => Backend::url('keios/monitor/websites'),
                        'permissions' => ['keios.monitor.websites'],
                    ],
                    'processes' => [
                        'label'       => 'keios.monitor::lang.app.processes',
                        'icon'        => 'icon-cubes',
                        'url'         => Backend::url('keios/monitor/processes'),
                        'permissions' => ['keios.monitor.processes'],
                    ],
                    'configchecker' => [
                        'label'       => 'keios.monitor::lang.app.check_config',
                        'icon'        => 'icon-gears',
                        'url'         => Backend::url('keios/monitor/configchecker'),
                        'permissions' => ['keios.monitor.checkconfig'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'Keios Monitor',
                'icon'        => 'icon-server',
                'description' => 'Configure monitoring',
                'class'       => 'Keios\Monitor\Models\Settings',
                'category'    => 'DevOps',
                'permissions' => ['keios.monitor.settings'],
            ],
        ];
    }


}
