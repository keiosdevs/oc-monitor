/*
 elementTransitions.js
 */
var PageTransitions = (function ($) {
    var startElement = 0,
        animEndEventNames = {
            'WebkitAnimation': 'webkitAnimationEnd',
            'OAnimation': 'oAnimationEnd',
            'msAnimation': 'MSAnimationEnd',
            'animation': 'animationend'
        };

    function getTransitionPrefix() {
        var b = document.body || document.documentElement;
        var s = b.style;
        var p = 'animation';
        if (typeof s[p] == 'string')
            return 'animation';

        // Tests for vendor specific prop
        v = ['Moz', 'Webkit', 'Khtml', 'O', 'ms'],
            p = p.charAt(0).toUpperCase() + p.substr(1);
        for (var i = 0; i < v.length; i++) {
            if (typeof s[v[i] + p] == 'string') {
                return v[i] + p;
            }
        }
        return false;
    }

    // animation end event name
    var animEndEventName = animEndEventNames[getTransitionPrefix()];

    function init() {
        $(".et-page").each(function () {
            $(this).data('originalClassList', $(this).attr('class'));
        });

        $(".et-wrapper").each(function () {
            $(this).data('current', 0);
            $(this).data('isAnimating', false);
            $(this).children(".et-page").eq(startElement).addClass('et-page-current');
        });

        $(".et-rotate-prev").click(function () {
            moveToPrevPage($(this));
        });

        $(".et-rotate-next").click(function () {
            moveToNextPage($(this));
        });

        $('.pg-setup-next-btn').parent().addClass('nav-btn-active');
        enumeratePages(0, $('.et-wrapper').children('.et-page').length);
        normalizeHeight();
        executePage(0);
    }

    function executePage(number) {
        if ($('.page-' + number).data('ready')) {
            return;
        }
        switch (number) {
            case 0:
                $('.page-0').data('ready', true);
                break;
            case 1:
                var list = $('#systemCheckList');
                var info = $('#systemCheckInfo');
                var timer = 500;

                $.request('onSystemCheck', {
                    success: function (data) {
                        function increaseTimer() {
                            timer += 300;
                        }

                        if (data.user.exists && data.user.isProUser) {
                            setTimeout(function () {
                                list.append('<li class="animated-content move_right animate fade_in pass">Keios.ProUser plugin detected</li>');
                                normalizeHeight();
                            }, timer);
                            increaseTimer();
                        } else if (data.user.exists) {
                            setTimeout(function () {
                                list.append('<li class="animated-content move_right animate fade_in pass">RainLab.User plugin detected</li>');
                                normalizeHeight();
                            }, timer);
                            increaseTimer();
                        } else {
                            setTimeout(function () {
                                list.append('<li class="animated-content move_right animate fade_in fail">User plugin not found</li>');
                                info.append('<div class="callout callout-warning">' +
                                    '<h4>Warning</h4>' +
                                    '<p>' +
                                    'Payment Gateway Installer couldn\'t find neither <a class="pg-link" href="#">Rainlab User</a>' +
                                    'nor' +
                                    '<a class="pg-link" href="#">Keios ProUser</a> plugin.<br/>' +
                                    'You can proceed, but User-based functionalities will be unavailable. </p>' +
                                    '</p>' +
                                    '</div>');
                                normalizeHeight();
                            }, timer);
                            increaseTimer();
                        }

                        var operatorExtensions = [];
                        data.operators.forEach(function (operatorObject) {
                            if (operatorObject.extension) {
                                operatorExtensions.push(operatorObject.name);
                            }
                        });

                        if (operatorExtensions.length > 0) {
                            operatorExtensions.forEach(function (operator) {
                                setTimeout(function () {
                                    list.append('<li class="animated-content move_right animate fade_in pass">' + operator + ' support detected</li>');
                                    normalizeHeight();
                                }, timer);
                                increaseTimer();
                            });
                        } else {
                            setTimeout(function () {
                                list.append('<li class="animated-content move_right animate fade_in fail">No Payment Operators found!</li>');
                                normalizeHeight();
                            }, timer);
                            setTimeout(function () {
                                info.append('<div class="callout callout-warning animate fade_in">' +
                                    '<h4>Warning</h4>' +
                                    '<p>Payment Gateway Installer couldn\'t find any Payment Operator extensions.<br/>' +
                                    'You can proceed and install additional operator extensions from October Market later. ' +
                                    'You can find payment operator extensions <a class="pg-link" href="#">here</a>.' +
                                    '</p>' +
                                    '</div>');
                                normalizeHeight();
                            }, timer);
                            increaseTimer();
                        }

                        $('#system-checkup').data('ready', true);
                    }
                });
                break;
            case 2:
                $('input[name="ordersEnabled"]').change(function () {

                    if ($(this).prop('checked')) {
                        $('#shippings').removeClass('hidden');
                        $('#useQuotes').removeClass('hidden');
                        $('#useUnderway').removeClass('hidden');
                    } else {
                        $('#shippings').addClass('hidden');
                        $('#useQuotes').addClass('hidden');
                        $('#useUnderway').addClass('hidden');
                        $('input[name="shippingsEnabled"]').attr('checked', false);
                        $('input[name="useQuotes"]').attr('checked', false);
                        $('input[name="useUnderway"]').attr('checked', false);
                    }
                });
                $('.page-2').data('ready', true);
                break;
            case 3:
                $('.page-3').data('ready', true);
                break;
            case 4:
                $('.page-4').data('ready', true);
                $('.saveBtn').click(function () {
                    var data = getFormData();
                    $.request('onSave', {
                        data: data
                    });
                });
                break;
        }
    }

    function getFormData() {
        return {
            ordersEnabled: $('input[name="ordersEnabled"]').prop('checked'),
            shippingsEnabled: $('input[name="shippingsEnabled"]').prop('checked'),
            useQuotes: $('input[name="useQuotes"]').prop('checked'),
            useUnderway: $('input[name="useUnderway"]').prop('checked'),
            frontendPreferLocal: $('input[name="frontendPreferLocal"]').prop('checked'),
            backendPreferLocal: $('input[name="backendPreferLocal"]').prop('checked'),
            defaultCurrency: $('select[name="defaultCurrency"]').val(),
            paymentSentToCanceledTimeout: $('input[name="paymentSentToCanceledTimeout"]').val(),
            paymentCreatedToCanceledTimeout: $('input[name="paymentCreatedToCanceledTimeout"]').val(),
            orderPlacedToCanceledTimeout: $('input[name="orderPlacedToCanceledTimeout"]').val(),
            orderShippedToFinishedTimeout: $('input[name="orderShippedToFinishedTimeout"]').val()
        }
    }

    function enumeratePages(current, pagesCount) {
        var $enum = $('#page-enum');
        $enum.empty();
        for (var i = 0; i < pagesCount; i++) {
            if (i == current) {
                $enum.append('<i class="icon icon-dot-circle-o"></i>');
            } else {
                $enum.append('<i class="icon icon-circle-o"></i>');
            }
        }
    }

    function getCurrentPageHeight() {
        return $('.et-page-current').outerHeight();
    }

    function setWrapperHeight(pageHeight, percent) {
        if (percent) {
            $('.et-wrapper')[0].style.height = pageHeight;
        } else {
            $('.et-wrapper').height(pageHeight + 60);
        }
    }

    function normalizeHeight() {
        setWrapperHeight(getCurrentPageHeight());
    }

    function hideNav() {
        $('.nav-btn').hide();
    }

    function showNav() {
        $('.nav-btn').show();
    }

    function determineButtonState(current, pagesCount) {
        if (current !== 0) {
            $('.pg-setup-prev-btn').parent().addClass('nav-btn-active');
        } else {
            $('.pg-setup-prev-btn').parent().removeClass('nav-btn-active');
        }

        if (current !== (pagesCount - 1) && $('.et-page-current').data('ready')) {
            $('.pg-setup-next-btn').parent().addClass('nav-btn-active');
        } else {
            $('.pg-setup-next-btn').parent().removeClass('nav-btn-active');
        }
    }

    function moveToPrevPage(block, callback) {
        var wrapper = $(block).closest('.et-wrapper');
        var current = wrapper.data('current');
        var pagesCount = wrapper.children('.et-page').length;

        if (current !== 0) {
            prevPage(wrapper, $(block).attr('et-out'), $(block).attr('et-in'), callback);
            determineButtonState(current - 1, pagesCount);
            enumeratePages(current - 1, pagesCount);
        }
    }

    function moveToNextPage(block, callback) {
        var wrapper = $(block).closest('.et-wrapper');
        var current = wrapper.data('current');
        var pagesCount = wrapper.children('.et-page').length;

        var pageReady = $('.et-page-current').data('ready');

        if (current !== (pagesCount - 1) && pageReady) {
            nextPage(wrapper, $(block).attr('et-out'), $(block).attr('et-in'), callback);
            determineButtonState(current + 1, pagesCount);
            enumeratePages(current + 1, pagesCount);
            executePage(current + 1);
        } else {

        }
    }

    function nextPage(block, outClass, inClass, callback) {
        block = $(block);
        inClass = formatClass(inClass);
        outClass = formatClass(outClass);
        var current = block.data('current'),
            $pages = block.children('.et-page'),
            pagesCount = $pages.length,
            endCurrPage = false,
            endNextPage = false;

        if (block.data('isAnimating')) {
            return false;
        }

        hideNav();
        block.data('isAnimating', true);

        var $currPage = $pages.eq(current);
        if (current < pagesCount - 1) {
            current++;
        }
        else {
            current = 0;
        }
        block.data('current', current);

        var $nextPage = $pages.eq(current).addClass('et-page-current');

        setWrapperHeight('100%', true);

        $currPage.addClass(outClass).on(animEndEventName, function () {
            $currPage.off(animEndEventName);
            endCurrPage = true;
            if (endNextPage) {
                if (jQuery.isFunction(callback)) {
                    callback(block, $nextPage, $currPage);
                }
                onEndAnimation($currPage, $nextPage, block);
            }
        });

        $nextPage.addClass(inClass).on(animEndEventName, function () {
            $nextPage.off(animEndEventName);
            endNextPage = true;
            if (endCurrPage) {
                onEndAnimation($currPage, $nextPage, block);
            }
        });
    }

    function prevPage(block, outClass, inClass, callback) {
        block = $(block);
        inClass = formatClass(inClass);
        outClass = formatClass(outClass);
        var current = block.data('current'),
            $pages = block.children('.et-page'),
            pagesCount = $pages.length,
            endCurrPage = false,
            endPrevPage = false;

        if (block.data('isAnimating')) {
            return false;
        }

        hideNav();
        block.data('isAnimating', true);

        var $currPage = $pages.eq(current);
        if (current - 1 > -1) {
            current--;
        }
        else {
            current = pagesCount - 1;
        }
        block.data('current', current);

        var $prevPage = $pages.eq(current).addClass('et-page-current');

        setWrapperHeight('100%', true);

        $currPage.addClass(outClass).on(animEndEventName, function () {
            $currPage.off(animEndEventName);
            endCurrPage = true;
            if (endPrevPage) {
                if (jQuery.isFunction(callback)) {
                    callback(block, $prevPage, $currPage);
                }
                onEndAnimation($currPage, $prevPage, block);
            }
        });

        $prevPage.addClass(inClass).on(animEndEventName, function () {
            $prevPage.off(animEndEventName);
            endPrevPage = true;
            if (endCurrPage) {
                onEndAnimation($currPage, $prevPage, block);
            }
        });
    }

    function onEndAnimation($outpage, $inpage, block) {
        resetPage($outpage, $inpage);
        normalizeHeight();
        block.data('isAnimating', false);
        showNav();
    }

    function resetPage($outpage, $inpage) {
        $outpage.attr('class', $outpage.data('originalClassList'));
        $inpage.attr('class', $inpage.data('originalClassList') + ' et-page-current');
    }

    function formatClass(str) {
        classes = str.split(" ");
        output = "";
        for (var n = 0; n < classes.length; n++) {
            output += " pt-page-" + classes[n];
        }
        return output;
    }

    return {
        init: init,
        nextPage: nextPage,
        animate: moveToNextPage
    };
})(jQuery);

jQuery(function ($) {
    PageTransitions.init();
});