if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
        if (permission === "granted") {
            var notification = new Notification("Nice! We will notify you about services being down.");
        }
    });
}

function playAlert() {
    var audio = new Audio('/plugins/keios/monitor/assets/audio/notif.mp4');
    audio.play();
}

function notifyAboutServiceDown(message) {
    if (!("Notification" in window)) {
        console.log("No notification support :(");
    }

    else if (Notification.permission === "granted") {
        playAlert();
        var notification = new Notification(message, {
            icon: '/plugins/keios/monitor/assets/img/logo.png'
        });
    }

    else if (Notification.permission !== 'denied') {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                playAlert();
                var notification = new Notification("Nice! We will notify you about services being down.", {
                    icon: '/plugins/keios/monitor/assets/img/logo.png'
                });
            }
        });
    }
}

function triggerNotification(data) {
    var changes = data.changes;
    if (changes) {
        for (var key in changes) {
            var status = changes[key];
            var message = key + " is " + status;
            notifyAboutServiceDown(message);
        }
    }
}

$('document').ready(function () {
    window.setInterval(function () {
        $('.loading-image').show();
        $('#servers-form').request('onUpdateServers', {
            update: {'km_livemonitor::servers': '#servers'},
            success: function (data) {
                triggerNotification(data);
                this.success(data);
            }
        });

        $('#websites-form').request('onUpdateWebsites', {
            update: {'km_livemonitor::websites': '#websites'},
            success: function (data) {
                triggerNotification(data);
                this.success(data);
            }
        });

        $('#processes-form').request('onUpdateProcesses', {
            update: {'km_livemonitor::processes': '#processes'},
            success: function (data) {
                triggerNotification(data);
                this.success(data);
            }
        });

    }, 10000);

    window.setInterval(function () {
        $('.loading-image').show();
        $.request('onLoadTodayStats', {
            update: {
                'km_charts::today': '#today-chart'
            }
        });
        $.request('onLoadWeekStats', {
            update: {
                'km_charts::week': '#week-chart'
            }
        });
        $.request('onLoadMonthStats', {
            update: {
                'km_charts::month': '#month-chart'
            }
        });
    }, 60000);

    window.setInterval(function () {
        $('.loading-image').show();
        $.request('onRefreshLogs', {
            update: {
                'km_logwidget::default': '#log-widget'
            }
        });
    }, 60000);

    $('#processes').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
    $('#websites').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
    $('#servers').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
    $('#week-chart').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
    $('#month-chart').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
    $('#day-chart').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });

    $('#log-widget').on('ajaxUpdate', function () {
        $('.loading-image').hide();
    });
});
