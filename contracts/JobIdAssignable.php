<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/15/16
 * Time: 4:42 PM
 */

namespace Keios\Monitor\Contracts;


interface JobIdAssignable
{
    public function assignJobId($id);
}