<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/15/16
 * Time: 4:43 PM
 */

namespace Keios\Monitor\Contracts;


interface JobStatus
{
    const IN_QUEUE = 0;
    const IN_PROGRESS = 1;
    const COMPLETE = 2;
    const ERROR = 3;
    const STOPPED = 4;
}