<?php namespace Keios\Monitor\Components;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Redirect;
use Keios\Monitor\Classes\CacheInvalidator;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Classes\ProcessRepository;
use Keios\Monitor\Classes\ServerRepository;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Jobs\CheckProcessJob;
use Keios\Monitor\Jobs\CheckServersJob;
use Keios\Monitor\Jobs\CheckWebsitesJob;
use Keios\Monitor\Models\Process;
use Keios\Monitor\Models\Server;
use Keios\Monitor\Models\Website;
use October\Rain\Exception\ApplicationException;
use RainLab\User\Models\User;
use Cms\Classes\Page;

/**
 * Class AddService
 *
 * @package Keios\Monitor\Components
 */
class ManageServices extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.addservice.name',
            'description' => 'keios.monitor::lang.components.addservice.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'keios.monitor::lang.app.redirect_to',
                'description' => 'keios.monitor::lang.app.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => '',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getRedirectOptions()
    {
        return ['' => '- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * @var ServerRepository
     */
    protected $serverRepo;
    /**
     * @var WebsiteRepository
     */
    protected $websiteRepo;
    /**
     * @var ProcessRepository
     */
    protected $processRepo;

    /**
     * @var JobManager
     */
    protected $jobManager;

    /**
     * AddService constructor.
     *
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->serverRepo = new ServerRepository();
        $this->websiteRepo = new WebsiteRepository();
        $this->processRepo = new ProcessRepository();
        $this->jobManager = \App::make('Keios\Monitor\Classes\JobManager');
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    public function onRun()
    {
        $user = \Auth::getUser();
        $this->page['monitor_profile'] = $user->monitorprofile;
        $this->page['websites'] = $this->websiteRepo->getAllUserWebsites($user->id);
        $this->page['processes'] = $this->processRepo->getAllUserProcesses($user->id);
        $this->page['servers'] = $this->serverRepo->getAllUserServers($user->id);
        
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    public function onLoadForm()
    {
        $data = post();
        $user = \Auth::getUser();
        $this->page['service_type'] = $data['service_type'];
        if ($data['service_type'] === 'process') {
            $hosts = $this->serverRepo->getEnabledUserServers($user->id);
            $this->page['user_hosts'] = $hosts;
        }
    }

    /**
     *
     * @throws \ApplicationException
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onRemoveService()
    {
        $data = post();
        $user = \Auth::getUser();
        // todo validation
        $type = $data['service_type'];
        $this->checkIfCanUpdate($user, $type);
        $id = $this->removeService($data, $user);
        \Log::info('Monitoring for '.$type.' service, id '.$id.' has been created');
        \Flash::success(
            'Service removed successfully. You may need to wait up to 1 minute for it to disappear from monitoring'
        );

        return $this->makeRedirection();
    }

    /**
     * @return null
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \InvalidArgumentException
     */
    public function onAddService()
    {
        $user = \Auth::getUser();

        $data = post();
        // todo validation

        $type = $data['service_type'];
        $this->checkIfCanCreate($user, $type);
        $id = $this->createService($data, $user);
        $this->page['service_type'] = $type;
        \Log::info('Monitoring for '.$type.' service, id '.$id.' has been created');
        \Flash::success(
            'Service created successfully. You may need to wait up to 1 minute for it to appear in monitoring'
        );

        return $this->makeRedirection();
    }


    /**
     *
     * @throws \ApplicationException
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function onUpdateService()
    {
        $user = \Auth::getUser();
        $data = post();
        // todo validation
        $type = $data['service_type'];
        $this->checkIfCanUpdate($user, $type);
        $id = $this->updateService($data, $user);
        \Log::info('Monitoring for '.$type.' service, id '.$id.' has been created');
        \Flash::success(
            'Service updated successfully. You may need to wait up to 1 minute for it to appear in monitoring'
        );

        return $this->makeRedirection();
    }

    /**
     * @param array $data
     * @param User $user
     *
     * @return null|int
     * @throws \ApplicationException
     */
    protected function removeService($data, $user)
    {
        $type = $data['service_type'];
        $id = null;
        $cacheInvalidator = new CacheInvalidator();
        if ($type === 'server') {
            $id = $data['server_id'];
            $this->checkIfServiceOwned($user->id, $id, 'server');
            Server::where('id', $id)->delete();
            $cacheInvalidator->clearServersCache($id, $user);
            $this->page['success'] = true;
        } elseif ($type === 'website') {
            $id = $data['website_id'];
            $this->checkIfServiceOwned($user->id, $id, 'website');
            Website::where('id', $id)->delete();
            $cacheInvalidator->clearWebsitesCache($id, $user);
            $this->page['success'] = true;
        } elseif ($type === 'process') {
            $id = $data['process_id'];
            $this->checkIfServiceOwned($user->id, $id, 'process');
            Process::where('id', $id)->delete();
            $cacheInvalidator->clearProcessCache($id, $user);
            $this->page['success'] = true;
        }

        return $id;
    }

    /**
     * @param array $data
     * @param User  $user
     *
     * @return int|null
     */
    protected function createService($data, $user)
    {
        $type = $data['service_type'];
        $id = null;
        if ($type === 'server') {
            $id = $this->updateServer($data, $user);
            $this->page['success'] = true;
            $this->issueCheckServers();
        } elseif ($type === 'website') {
            $id = $this->updateWebsite($data, $user);
            $this->page['success'] = true;
            $this->issueCheckWebsites();
        } elseif ($type === 'process') {
            $id = $this->updateProcess($data, $user);
            $this->page['success'] = true;
            $this->issueCheckProcesses();
        }

        return $id;
    }

    /**
     * @param array $data
     * @param User  $user
     *
     * @return int|null
     * @throws \ApplicationException
     */
    protected function updateService($data, $user)
    {
        $type = $data['service_type'];
        $resultId = null;
        if ($type === 'server') {
            $id = $data['server_id'];
            $this->checkIfServiceOwned($user->id, $id, 'server');
            $server = $this->serverRepo->getServerById($id);
            $resultId = $this->updateServer($data, $user, $server);
            $this->page['success'] = true;
            $this->issueCheckServers();
        } elseif ($type === 'website') {
            $id = $data['website_id'];
            $this->checkIfServiceOwned($user->id, $id, 'website');
            $website = $this->websiteRepo->getWebsiteById($id);
            $resultId = $this->updateWebsite($data, $user, $website);
            $this->page['success'] = true;
            $this->issueCheckWebsites();
        } elseif ($type === 'process') {
            $id = $data['process_id'];
            $this->checkIfServiceOwned($user->id, $id, 'process');
            $process = $this->processRepo->getProcessById($id);
            $resultId = $this->updateProcess($data, $user, $process);
            $this->page['success'] = true;
            $this->issueCheckProcesses();
        }

        return $resultId;
    }


    /**
     * @param array   $data
     * @param User    $user
     * @param Process $process
     *
     * @return mixed|string
     */
    private function updateProcess($data, $user, $process = null)
    {
        $create = false;
        if (!$process) {
            $process = new Process();
            $create = true;
        }
        $isEnabled = false;
        if (isset($data['is_enabled'])) {
            $isEnabled = true;
        }
        $process->server_id = $data['server'];
        $process->process_name = $data['process_name'];
        $process->process_port = $data['process_port'];
        $process->check_using = $data['check_using'];
        $process->is_enabled = $isEnabled;
        $process->is_online = false;
        $process->send_email = true;
        $process->save();

        if ($create) {
            \DB::table('keios_monitor_user_processes')->insert(['process_id' => $process->id, 'user_id' => $user->id]);
        }

        return $process->id;
    }

    /**
     * @param array   $data
     * @param User    $user
     * @param Website $website
     *
     * @return mixed|string
     */
    private function updateWebsite($data, $user, $website = null)
    {
        $create = false;
        if (!$website) {
            $website = new Website();
            $create = true;
        }
        $isEnabled = false;
        if (isset($data['is_enabled'])) {
            $isEnabled = true;
        }
        $website->url = $data['url'];
        $website->is_enabled = $isEnabled;
        $website->is_online = false;
        $website->send_email = true;
        $website->save();
        if ($create) {
            \DB::table('keios_monitor_user_websites')->insert(['website_id' => $website->id, 'user_id' => $user->id]);
        }

        return $website->id;
    }

    /**
     * @return int
     */
    private function issueCheckWebsites()
    {
        $user = \Auth::getUser();
        $job = new CheckWebsitesJob($user->id);
        $parameters = [];

        return $this->jobManager->dispatch($job, 'check_websites', $parameters);
    }

    /**
     * @return int
     */
    private function issueCheckProcesses()
    {
        $user = \Auth::getUser();
        $job = new CheckProcessJob($user->id);
        $parameters = [];

        return $this->jobManager->dispatch($job, 'check_processes', $parameters);
    }

    /**
     * @return int
     */
    private function issueCheckServers()
    {
        $user = \Auth::getUser();
        $job = new CheckServersJob($user->id);
        $parameters = [];

        return $this->jobManager->dispatch($job, 'check_servers', $parameters);
    }


    /**
     * @param array       $data
     * @param User        $user
     *
     * @param Server|null $server
     *
     * @return mixed|string
     */
    private function updateServer($data, $user, $server = null)
    {
        $isEnabled = false;
        if (isset($data['is_enabled'])) {
            $isEnabled = true;
        }
        $create = false;
        if (!$server) {
            $server = new Server();
            $create = true;
        }

        $server->host = $data['host'];
        $server->ssh_port = $data['ssh_port'];
        $server->ssh_user = $data['ssh_user'];
        $server->is_enabled = $isEnabled;
        $server->is_online = false;
        $server->send_email = true;
        $server->save();
        if ($create) {
            \DB::table('keios_monitor_user_servers')->insert(['server_id' => $server->id, 'user_id' => $user->id]);
        }

        return $server->id;
    }

    /**
     * @param array $data
     *
     * @throws \ValidationException
     */
    protected function validateServerData($data)
    {
        $rules = [
            'host'       => 'required',
            'ssh_port'   => 'required',
            'ssh_user'   => 'required',
            'is_enabled' => 'required',
        ];
        $validate = \Validator::make($data, $rules);
        if ($validate->fails()) {
            throw new \ValidationException($validate);
        }
    }

    /**
     * @param array $data
     *
     * @throws \ValidationException
     */
    protected function validateProcessData($data)
    {
        $rules = [
            'server'     => 'required',
            'ssh_port'   => 'required',
            'ssh_user'   => 'required',
            'is_enabled' => 'required',
        ];
        $validate = \Validator::make($data, $rules);
        if ($validate->fails()) {
            throw new \ValidationException($validate);
        }
    }

    /**
     * @param array $data
     *
     * @throws \ValidationException
     */
    protected function validateWebsiteData($data)
    {
        $rules = [
            'url'        => 'required',
            'is_enabled' => 'required',
        ];
        $validate = \Validator::make($data, $rules);
        if ($validate->fails()) {
            throw new \ValidationException($validate);
        }
    }

    /**
     * @param int    $userId
     * @param int    $serviceId
     * @param string $serviceType
     * @param bool   $checkBackend
     *
     * @return bool
     * @throws \ApplicationException
     */
    protected function checkIfServiceOwned($userId, $serviceId, $serviceType, $checkBackend = true)
    {
        $backendUser = \BackendAuth::getUser();
        //todo roles
        if ($checkBackend && $backendUser && $backendUser->is_superuser) {
            return true;
        }
        $service = null;
        if ($serviceType === 'server') {
            $service = $this->serverRepo->getServerById($serviceId);
        } elseif ($serviceType === 'website') {
            $service = $this->websiteRepo->getWebsiteById($serviceId);
        } elseif ($serviceType === 'process') {
            $service = $this->processRepo->getProcessById($serviceId);
        }
        if ($service) {
            foreach ($service->users as $user) {
                if ($user->id === $userId) {
                    return true;
                }
            }
        }

        throw new \ApplicationException('Service not owned by current user');
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     *
     * @return Redirect|null
     */
    protected function makeRedirection()
    {
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return \Redirect::to($redirectUrl);
        }

        return null;
    }

    /**
     * @param User   $user
     * @param string $service
     *
     * @return bool
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \InvalidArgumentException
     */
    protected function checkIfCanCreate($user, $service)
    {
        $this->checkIfCanUpdate($user, $service);
        $profile = $user->monitorprofile;
        $processesLimit = $profile->processes_limit;
        $serversLimit = $profile->servers_limit;
        $websitesLimit = $profile->websites_limit;

        if ($service === 'process') {
            $has = $this->processRepo->getAllUserProcesses($user->id, false);
            if (count($has) >= $processesLimit) {
                throw new ApplicationException('You cannot add more processes! Contact administrator.');
            }
        } elseif ($service === 'server') {
            $has = $this->serverRepo->getAllUserServers($user->id, false);
            if (count($has) >= $serversLimit) {
                throw new ApplicationException('You cannot add more servers! Contact administrator.');
            }
        } elseif ($service === 'website') {
            $has = $this->websiteRepo->getAllUserWebsites($user->id, false);
            if (count($has) >= $websitesLimit) {
                throw new ApplicationException('You cannot add more websites! Contact administrator.');

            }
        }

        return true;
    }

    /**
     * @param User   $user
     * @param string $service
     *
     * @throws ApplicationException
     */
    protected function checkIfCanUpdate($user, $service)
    {
        $profile = $user->monitorprofile;
        $canEditProcesses = $profile->manage_processes;
        $canEditServers = $profile->manage_servers;
        $canEditWebsites = $profile->manage_websites;
        if ($service === 'process') {
            if (!$canEditProcesses) {
                throw new ApplicationException('You cannot add or edit processes.');
            }
        } elseif ($service === 'server') {
            if (!$canEditServers) {
                throw new ApplicationException('You cannot add or edit servers');
            }
        } elseif ($service === 'website') {
            if (!$canEditWebsites) {
                throw new ApplicationException('You cannot add or edit websites');
            }
        }
    }
}