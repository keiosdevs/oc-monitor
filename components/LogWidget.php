<?php namespace Keios\Monitor\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Illuminate\Pagination\LengthAwarePaginator;
use Keios\Monitor\Classes\LogFilter;
use Keios\Monitor\Classes\LogRepository;
use Keios\Monitor\Classes\ProcessRepository;
use Keios\Monitor\Classes\ServerRepository;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Models\LogEntry;

/**
 * Class LogWidget
 *
 * @package Keios\Monitor\Components
 */
class LogWidget extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.logwidget.name',
            'description' => 'keios.monitor::lang.components.logwidget.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'logs_amount' => [
                'title'       => 'Logs amount',
                'description' => 'How much logs to take to widget',
                'default'     => 10,
            ],
        ];
    }

    /**
     * @var ProcessRepository
     */
    protected $processRepo;
    /**
     * @var WebsiteRepository
     */
    protected $websiteRepo;
    /**
     * @var ServerRepository
     */
    protected $serverRepo;
    /**
     * @var LogRepository
     */
    protected $logRepo;

    /**
     * LogWidget constructor.
     *
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->processRepo = new ProcessRepository();
        $this->websiteRepo = new WebsiteRepository();
        $this->serverRepo = new ServerRepository();
        $this->logRepo = new LogRepository();
    }

    /**
     * @throws \InvalidArgumentException
     */
    public function onRun()
    {
        $logs = $this->getLogs();
        $this->page['logs'] = $logs;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public function onRefreshLogs()
    {
        $logs = $this->getLogs();
        $this->page['logs'] = $logs;
    }

    /**
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getLogs()
    {
        $user = \Auth::getUser();

        $logsDb = $this->getLogsFromDb($user);
        $this->setupPaginator($logsDb, $this->property('logs_amount'));
        $logs = [];
        foreach ($logsDb as $logDb) {
            $url = '<removed website>';
            $processName = '<removed process>';
            $host = '<removed server>';
            switch ($logDb->type) {
                case 1:
                    $process = $this->processRepo->getProcessById($logDb->process_id);
                    if ($process) {
                        $processName = $process->process_name;
                        $hostEntry = $this->serverRepo->getServerById($process->server_id);
                        $host = $hostEntry->host;
                    }
                    if ($logDb->was_online == 1) {
                        $message = $processName.'@'.$host.' '.trans(
                                'keios.monitor::lang.app.went_down'
                            );
                    } else {
                        $message = $processName.'@'.$host.' '.trans(
                                'keios.monitor::lang.app.went_up'
                            );
                    }
                    $logs[] = [
                        'date'    => $logDb->created_at->toDateTimeString(),
                        'message' => $message,
                        'type'    => 'process',
                    ];
                    break;
                case 2:
                    $server = $this->serverRepo->getServerById($logDb->server_id);
                    if ($server) {
                        $host = $server->host;
                    }
                    if ($logDb->was_online == 1) {
                        $message = $host.' '.trans('keios.monitor::lang.app.went_down');
                    } else {
                        $message = $host.' '.trans('keios.monitor::lang.app.went_up');
                    }
                    $logs[] = [
                        'date'    => $logDb->created_at->toDateTimeString(),
                        'message' => $message,
                        'type'    => 'server',
                    ];
                    break;
                case 3:
                    $website = $this->websiteRepo->getWebsiteById($logDb->website_id);
                    if ($website) {
                        $url = $website->url;
                    }
                    if ($logDb->was_online == 1) {
                        $message = $url.' '.trans('keios.monitor::lang.app.went_down');
                    } else {
                        $message = $url.' '.trans('keios.monitor::lang.app.went_up');
                    }
                    $logs[] = [
                        'date'    => $logDb->created_at->toDateTimeString(),
                        'message' => $message,
                        'type'    => 'website',
                    ];
                    break;
            }
        }

        return $logs;
    }

    /**
     * @param $user
     *
     * @return LengthAwarePaginator
     * @throws \InvalidArgumentException
     */
    public function getLogsFromDb($user)
    {


        $filters = $this->setupFilters();
        $this->page['filters'] = $filters;
        /** @var LengthAwarePaginator $logsDb */
        $logsDb = $this->logRepo->getUserLatestRecords(
            $filters
        );

        return $logsDb;
    }
    /**
     * @return LogFilter
     */
    public function setupFilters()
    {
        $data = get();
        $filters = new LogFilter();
        $filters->perPage = $this->property('logs_amount');
        if (array_key_exists('from', $data)) {
            $filters->from = new Carbon($data['from']);
        }
        if (array_key_exists('to', $data)) {
            $to = new Carbon($data['to']);
            $filters->to = $to->endOfDay();
        }
        if (array_key_exists('type', $data)) {
            $filters->type = $data['type'];
        }
        if (array_key_exists('name', $data)) {
            $filters->name = $data['name'];
        }

        return $filters;
    }

    /**
     * @param LengthAwarePaginator $logs
     * @param int                  $perPage
     */
    public function setupPaginator($logs, $perPage)
    {
        $data = get();
        if (array_key_exists('page', $data)) {
            $this->page['scroll_to_log'] = true;
        }
        $this->page['log_total'] = $logs->total();
        $this->page['log_per_page'] = $perPage;
        $this->page['log_total_pages'] = $logs->lastPage();
        $this->page['log_current_page'] = $logs->currentPage();
    }

    /**
     *
     */
    public function onLogsUpdate()
    {

    }

}