<?php namespace Keios\Monitor\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Illuminate\Support\Facades\Redirect;
use Keios\Monitor\Classes\JobManager;
use Keios\Monitor\Jobs\CheckProcessJob;
use Keios\Monitor\Jobs\CheckServersJob;
use Keios\Monitor\Jobs\CheckWebsitesJob;

/**
 * Class HelperComponent
 *
 * @package Keios\Monitor\Components
 */
class HelperComponent extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.helpercomponent.name',
            'description' => 'keios.monitor::lang.components.helpercomponent.description',
        ];
    }


    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'keios.monitor::lang.app.redirect_to',
                'description' => 'keios.monitor::lang.app.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => 'home',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getRedirectOptions()
    {
        return ['' => '- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     *
     */
    public function onDisableMailing()
    {
        $user = \Auth::getUser();
        $user->monitorprofile->enable_mailer = false;
        $user->monitorprofile->save();
        return \Redirect::to($this->page->url);
    }

    /**
     *
     */
    public function onEnableMailing()
    {
        $user = \Auth::getUser();
        $user->monitorprofile->enable_mailer = true;
        $user->monitorprofile->save();
        return \Redirect::to($this->page->url);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function onIssueRefresh()
    {
        try {
            $this->issueCheckServers();
            $this->issueCheckWebsites();
            $this->issueCheckProcesses();
        } catch (\Exception $e) {
            throw $e;
        }
        \Flash::success('Update issued! Results will be uploaded shortly.');

        return $this->makeRedirection();
    }

    /**
     * @var JobManager
     */
    protected $jobManager;

    /**
     * AddService constructor.
     *
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->jobManager = \App::make('Keios\Monitor\Classes\JobManager');

    }

    /**
     * @throws \ApplicationException
     */
    public function onRun()
    {
        $user = \Auth::getUser();
        if (class_exists('Keios\Apparatus\Plugin')) {
            $this->page['apparatus_enabled'] = true;
        }
        if (!$user) {
            throw new \ApplicationException(
                'User not found! Configure User Session component or use MonitorTheme: https://bitbucket.org/keiosdevs/oc-monitor-theme'
            );
        }
    }

    /**
     * @return int
     */
    private function issueCheckWebsites()
    {
        $user = \Auth::getUser();
        $job = new CheckWebsitesJob($user->id);
        $parameters = [];

        $this->jobManager->dispatch($job, 'check_websites', $parameters);
    }

    /**
     * @return int
     */
    private function issueCheckProcesses()
    {
        $user = \Auth::getUser();
        $job = new CheckProcessJob($user->id);
        $parameters = [];

        $this->jobManager->dispatch($job, 'check_processes', $parameters);
    }

    /**
     * @return int
     */
    private function issueCheckServers()
    {
        $user = \Auth::getUser();
        $job = new CheckServersJob($user->id);
        $parameters = [];

        $this->jobManager->dispatch($job, 'check_servers', $parameters);
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     *
     * @return Redirect|null
     */
    protected function makeRedirection()
    {
        $redirectUrl = $this->pageUrl($this->property('redirect'))
            ?: $this->property('redirect');

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return \Redirect::to($redirectUrl);
        }

        return null;
    }

}