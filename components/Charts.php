<?php namespace Keios\Monitor\Components;

use Carbon\Carbon;
use Cms\Classes\ComponentBase;
use Keios\Monitor\Classes\LogFilter;
use Keios\Monitor\Classes\LogRepository;
use Keios\Monitor\Models\LogEntry;

class Charts extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.charts.name',
            'description' => 'keios.monitor::lang.components.charts.description',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    protected $logRepo;

    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->logRepo = new LogRepository();
    }

    public function onRun()
    {

    }

    public function onLoadTodayStats()
    {
        $now = Carbon::now();

        $counter = [
            1 => 0,
            2 => 0,
            3 => 0,
        ];
        
        $filter = new LogFilter();
        $filter->from = $now->copy()->startOfDay();
        $filter->to = $now->copy()->addHour();
        
        $todayLogs = $this->logRepo->getUserLatestRecords($filter);

        $this->page['today_logs'] = $todayLogs;

        $hourInterval = new \DateInterval('PT1H');

        $dayPeriod = new \DatePeriod($now->copy()->subHours(23), $hourInterval, $now->copy()->addHour());
        $hours = [];
        /** @var Carbon $day */
        foreach ($dayPeriod as $day) {
            $hours[$day->format('m-d-Y-H')] = [
                1 => 0,
                2 => 0,
                3 => 0,
            ];
        }
        /** @var LogEntry[] $todayLogs */
        foreach ($todayLogs as $log) {
            $created = $log->created_at->format('m-d-Y-H');
            if (array_key_exists($created, $hours)) {
                ++$hours[$created][$log->type];
                ++$counter[$log->type];
            }
        }
        $day = [];
        foreach ($hours as $key => $value) {
            $day[substr($key, -2)] = $value;
        }
        $this->page['today_server'] = $counter[2];
        $this->page['today_process'] = $counter[1];
        $this->page['today_websites'] = $counter[3];
        $this->page['today_crash_count'] = $counter[1] + $counter[2] + $counter[3];
        $this->page['day'] = $day;
    }

    public function onLoadWeekStats()
    {
        $counter = [
            1 => 0,
            2 => 0,
            3 => 0,
        ];
        $now = Carbon::now();
        $dayInterval = new \DateInterval('P1D');
        $thisWeek = $now->copy()->subDays(7);

        $filter = new LogFilter();
        $filter->from = $thisWeek;
        $filter->to = $now;

        $weekLogs = $this->logRepo->getUserLatestRecords($filter);
        $this->page['week_logs'] = $weekLogs;
        $weekPeriod = new \DatePeriod($thisWeek, $dayInterval, $now->copy()->addDay()->startOfDay());
        $days = [];
        /** @var Carbon $day */
        foreach ($weekPeriod as $day) {
            $days[$day->toDateString()] = [
                1 => 0,
                2 => 0,
                3 => 0,
            ];
        }

        /** @var LogEntry[] $weekLogs */
        foreach ($weekLogs as $log) {
            $created = $log->created_at->toDateString();
            if (array_key_exists($created, $days)) {
                ++$days[$created][$log->type];
                ++$counter[$log->type];
            }
        }
        $this->page['week_server'] = $counter[2];
        $this->page['week_process'] = $counter[1];
        $this->page['week_websites'] = $counter[3];
        $this->page['week_crash_count'] = $counter[1] + $counter[2] + $counter[3];
        $this->page['week'] = $days;

    }

    public function onLoadMonthStats()
    {
        $counter = [
            1 => 0,
            2 => 0,
            3 => 0,
        ];
        $now = Carbon::now();
        $dayInterval = new \DateInterval('P1D');
        $thisMonth = $now->copy()->subMonth();

        $filter = new LogFilter();
        $filter->from = $thisMonth;
        $filter->to = $now;
        $monthLogs = $this->logRepo->getUserLatestRecords($filter);
        $this->page['month_logs'] = $monthLogs;
        $monthPeriod = new \DatePeriod($thisMonth, $dayInterval, $now->copy()->addDay()->startOfDay());
        $days = [];
        /** @var Carbon $day */
        foreach ($monthPeriod as $day) {
            $days[$day->toDateString()] = [
                1 => 0,
                2 => 0,
                3 => 0,
            ];
        }
        /** @var LogEntry[] $monthLogs */
        foreach ($monthLogs as $log) {
            $created = $log->created_at->toDateString();
            if (array_key_exists($created, $days)) {
                ++$days[$created][$log->type];
                ++$counter[$log->type];
            }
        }
        $this->page['month_server'] = $counter[2];
        $this->page['month_process'] = $counter[1];
        $this->page['month_websites'] = $counter[3];
        $this->page['month_crash_count'] = $counter[1] + $counter[2] + $counter[3];
        $this->page['month'] = $days;
    }

}