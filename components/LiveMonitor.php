<?php namespace Keios\Monitor\Components;

use Cms\Classes\ComponentBase;
use Keios\Monitor\Classes\ProcessRepository;
use Keios\Monitor\Classes\ServerRepository;
use Keios\Monitor\Classes\WebsiteRepository;
use Keios\Monitor\Models\Process;
use Keios\Monitor\Models\Server;
use Keios\Monitor\Models\Website;

/**
 * Class LiveMonitor
 *
 * @package Keios\Monitor\Components
 */
class LiveMonitor extends ComponentBase
{

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.livemonitor.name',
            'description' => 'keios.monitor::lang.components.livemonitor.description',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [
            'enable_servers'   => [
                'title'       => 'Enable servers',
                'description' => 'Show servers in component',
                'type'        => 'dropdown',
                'options'     => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'default'     => 1,
            ],
            'enable_websites'  => [
                'title'       => 'Enable websites',
                'description' => 'Show websites in component',
                'type'        => 'dropdown',
                'options'     => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'default'     => 1,
            ],
            'enable_processes' => [
                'title'       => 'Enable processes',
                'description' => 'Show processes in component',
                'type'        => 'dropdown',
                'options'     => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'default'     => 1,
            ],
        ];
    }

    /**
     * @var ServerRepository
     */
    protected $serversRepo;

    /**
     * @var WebsiteRepository
     */
    protected $websitesRepo;

    /**
     * @var ProcessRepository
     */
    protected $processesRepo;

    /**
     * LiveMonitor constructor.
     *
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->serversRepo = new ServerRepository();
        $this->websitesRepo = new WebsiteRepository();
        $this->processesRepo = new ProcessRepository();
    }

    /**
     * Add js and set variables on page load
     *
     * @throws \InvalidArgumentException
     */
    public function onRun()
    {
        $this->addJs('assets/js/livemonitor.js');
        $this->addCss('assets/css/livemonitor.css');

        $this->setPageVariables();
    }

    /**
     * Set all variables per enabled properties
     *
     * @throws \InvalidArgumentException
     */
    protected function setPageVariables()
    {
        $user = \Auth::getUser();
        $enableServers = $this->property('enable_servers');
        $enableWebsites = $this->property('enable_websites');
        $enableProcesses = $this->property('enable_processes');

        $this->page['enable_servers'] = $enableServers;
        $this->page['enable_websites'] = $enableWebsites;
        $this->page['enable_processes'] = $enableProcesses;
        $servers = [];
        $serversPerIds = [];
        if ($enableProcesses || $enableProcesses) {
            $servers = $this->serversRepo->getEnabledUserServers($user->id);
            foreach ($servers as $server) {
                $serversPerIds[$server->id] = $server->host;
            }
        }

        if ($enableServers) {
            $this->page['servers'] = $servers;
        }
        if ($enableWebsites) {
            $this->page['websites'] = $this->websitesRepo->getEnabledUserWebsites($user->id);

        }
        if ($enableProcesses) {
            $this->page['processes'] = $this->processesRepo->getEnabledUserProcesses($user->id);
            $this->page['process_servers'] = $serversPerIds;
        }
    }

    /**
     * Update servers on ajax call
     *
     * @throws \InvalidArgumentException
     */
    public function onUpdateServers()
    {
        $user = \Auth::getUser();
        $data = post();
        $enableServers = $this->property('enable_servers');

        if ($enableServers) {
            $this->page['servers'] = $servers = $this->serversRepo->getEnabledUserServers($user->id);

            return $this->checkServersStatusChange($data, $servers);
        }

        return [];
    }

    /**
     * Update websites on ajax call
     *
     * @throws \InvalidArgumentException
     */
    public function onUpdateWebsites()
    {
        $user = \Auth::getUser();
        $data = post();
        $enableWebsites = $this->property('enable_websites');

        if ($enableWebsites) {
            $this->page['websites'] = $websites = $this->websitesRepo->getEnabledUserWebsites($user->id);

            return $this->checkWebsitesStatusChange($data, $websites);
        }

        return [];
    }

    /**
     * Update processes on ajax call
     *
     * @throws \InvalidArgumentException
     */
    public function onUpdateProcesses()
    {
        $user = \Auth::getUser();
        $data = post();
        $enableProcesses = $this->property('enable_processes');

        if ($enableProcesses) {
            $this->page['processes'] = $processes = $this->processesRepo->getEnabledUserProcesses($user->id);

            return $this->checkProcessesStatusChange($data, $processes);
        }

        return [];
    }

    /**
     * @param array     $websitesPost
     * @param Website[] $websites
     *
     * @return array|\Redirect
     */
    private function checkWebsitesStatusChange(array $websitesPost, $websites)
    {
        $was = [];
        $is = [];
        foreach ($websitesPost as $website => $value) {
            $websiteId = str_replace('website-', '', $website);
            $was[$websiteId] = $value;
        }

        foreach ($websites as $website) {
            if (array_key_exists($website->id, $was) && $website->is_online != $was[$website->id]) {
                if ($website->is_online == 0) {
                    $is[$website->url] = 'down';
                } else {
                    $is[$website->url] = 'up';
                }
            }
            if (!array_key_exists($website->id, $was)) {
                \Artisan::call('monitor:check-website');

                return \Redirect::to('/');
            }
        }

        return ['changes' => $is];
    }

    /**
     * @param array     $processesPost
     * @param Process[] $processes
     *
     * @return array|\Redirect
     */
    private function checkProcessesStatusChange(array $processesPost, $processes)
    {
        $was = [];
        $is = [];
        foreach ($processesPost as $process => $value) {
            $processId = str_replace('process-', '', $process);
            $was[$processId] = $value;
        }

        foreach ($processes as $process) {
            if (array_key_exists($process->id, $was) && $process->is_online != $was[$process->id]) {
                if ($process->is_online == 0) {
                    $is[$process->process_name.'@'.$process->server->host] = 'down';
                } else {
                    $is[$process->process_name.'@'.$process->server->host] = 'up';
                }
            }
            if (!array_key_exists($process->id, $was)) {
                \Artisan::call('monitor:check-process');

                return \Redirect::to('/');
            }
        }

        return ['changes' => $is];
    }

    /**
     * @param array    $serversPost
     * @param Server[] $servers
     *
     * @return array|\Redirect
     */
    private function checkServersStatusChange(array $serversPost, $servers)
    {
        $was = [];
        $is = [];
        foreach ($serversPost as $server => $value) {
            $serverId = str_replace('server-', '', $server);
            $was[$serverId] = $value;
        }

        foreach ($servers as $server) {
            $x[] = $server->is_online;
            if (array_key_exists($server->id, $was) && $server->is_online != $was[$server->id]) {
                if ($server->is_online == 0) {
                    $is[$server->host] = 'down';
                } else {
                    $is[$server->host] = 'up';
                }
            }
            if (!array_key_exists($server->id, $was)) {
                \Artisan::call('monitor:check-server');

                return \Redirect::to('/');
            }
        }

        return ['changes' => $is];
    }


}