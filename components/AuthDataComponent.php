<?php namespace Keios\Monitor\Components;

use Cms\Classes\ComponentBase;
use Keios\Monitor\Classes\ServerRepository;
use Keios\Monitor\Classes\SshRunner;
use Keios\Monitor\Models\Settings;

class AuthDataComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'keios.monitor::lang.components.authdatacomponent.name',
            'description' => 'keios.monitor::lang.components.authdatacomponent.description',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    protected $serverRepo;

    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $this->serverRepo = new ServerRepository();
    }

    public function onRun()
    {
        $settings = Settings::instance();
        $keyPath = $settings->get('ssh_key_path');
        $user = \Auth::getUser();
        $servers = $this->serverRepo->getEnabledUserServers($user->id, true);
        $this->page['servers'] = $servers;
        if (file_exists($keyPath)) {
            $key = file_get_contents($keyPath.'.pub');
            $this->page['ssh_key'] = $key;
        } else {
            $this->page['ssh_key'] = 'NOT CONFIGURED';
        }

    }

    public function onAuthorizeSsh()
    {
        $data = post();
        $server = $this->serverRepo->getServerById($data['server']);
        $sshConnector = new SshRunner();
        $result = $sshConnector->runCommands($server->host, ['uname -a']);
        if (is_array($result) && $result[0]) {
            \Flash::success('Connection works fine! Uname: '.$result[0]);
        } else {
            \Log::error(print_r($result, true));
            \Flash::error('Problems with connection: ');
        }

        return $this->makeRedirection();
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     *
     * @return mixed
     */
    protected function makeRedirection()
    {
        return \Redirect::to($this->page->url);
    }
}