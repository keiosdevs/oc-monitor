<?php namespace Keios\Monitor\Classes;

use Collective\Remote\RemoteServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class SshServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'monitor.connections',
            function () {
                /** @noinspection OffsetOperationsInspection */
                return new SshConnectionConfigurator($this->app['config']);
            }
        );

        $this->app->register(RemoteServiceProvider::class);

        $alias = AliasLoader::getInstance();
        $alias->alias('SSH', 'Collective\Remote\RemoteFacade');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'monitor.connections',
        ];
    }

}
