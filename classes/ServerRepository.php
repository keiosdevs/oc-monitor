<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 9:54 AM
 */

namespace Keios\Monitor\Classes;

use Keios\Monitor\Models\Server;

/**
 * Class ServerRepository
 *
 * @package Keios\Monitor\Classes
 */
class ServerRepository
{

    /**
     * @param int  $userId
     * @param bool $recognizeRoot
     *
     * @return \Keios\Monitor\Models\Server[]
     * @throws \InvalidArgumentException
     */
    public function getEnabledUserServers($userId, $recognizeRoot = true)
    {
        return $this->getOwnedServers($userId, $recognizeRoot, true);
    }

    /**
     * @return Server[]
     */
    public function getEnabledServers()
    {
        return Server::where('is_enabled', 1)->rememberForever('km_enabled_servers')->get();
    }

    /**
     * @param string $term
     *
     * @return Server[]
     */
    public function getServersLike($term)
    {
        return Server::where('host', 'LIKE', "%$term%")->get();
    }

    /**
     *
     * @return Server[]
     */
    public function getAllServers()
    {
        return Server::rememberForever('km_servers')->get();
    }

    /**
     * @param integer $id
     *
     * @return Server
     */
    public function getServerById($id)
    {
        return Server::where('id', $id)->rememberForever('km_server_'.$id)->first();
    }

    /**
     * @param int  $userId
     *
     * @param bool $recognizeRoot
     *
     * @return \Keios\Monitor\Models\Server[]
     * @throws \InvalidArgumentException
     */
    public function getAllUserServers($userId, $recognizeRoot = true)
    {
        return $this->getOwnedServers($userId, $recognizeRoot, false);
    }


    /**
     * @param      $userId
     * @param bool $recognizeRoot
     *
     * @param bool $enabled
     *
     * @return array
     */
    protected function getOwnedServers($userId, $recognizeRoot = true, $enabled = false)
    {
        $backendUser = \BackendAuth::getUser();
        //todo roles
        if ($recognizeRoot && $backendUser && $backendUser->is_superuser) {
            /** @var \stdClass[] $ownedProcesses */
            if ($enabled) {
                $ownedServers = $this->getEnabledServers();
            } else {
                $ownedServers = $this->getAllServers();
            }
        } else {

            if ($enabled) {
                $ownedServers = Server::
                join('keios_monitor_user_servers', 'id', '=', 'keios_monitor_user_servers.server_id')
                    ->where('user_id', $userId)
                    ->rememberForever('km_servers_of_'.$userId)
                    ->get();
            } else {
                $ownedServers = Server::
                join('keios_monitor_user_servers', 'id', '=', 'keios_monitor_user_servers.server_id')
                    ->where('user_id', $userId)
                    ->where('is_enabled', 1)
                    ->rememberForever('km_en_servers_of_'.$userId)
                    ->get();
            }
        }

        return $ownedServers;
    }
}