<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 9:54 AM
 */

namespace Keios\Monitor\Classes;

use Keios\Monitor\Models\Website;

/**
 * Class WebsiteRepository
 *
 * @package Keios\Monitor\Classes
 */
class WebsiteRepository
{

    /**
     * @param string $url
     *
     * @return Website[]
     */
    public function getWebsitesByUrl($url)
    {
        return Website::where('is_enabled', 1)->where('url', $url)->get();
    }


    /**
     * @param string $term
     *
     * @return Website[]
     */
    public function getWebsitesLike($term)
    {
        return Website::where('url', 'LIKE', "%$term%")->get();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllWebsites()
    {
        return Website::rememberForever('km_websites')->get();
    }

    /**
     *
     * @param int $id
     *
     * @return Website
     */
    public function getWebsiteById($id)
    {

        return Website::where('id', $id)->rememberForever('km_website_'.$id)->first();
    }

    /**
     * @return Website[]
     */
    public function getEnabledWebsites()
    {
        return Website::where('is_enabled', 1)->rememberForever('km_enabled_websites')->get();
    }

    /**
     * @param int  $userId
     *
     * @param bool $recognizeRoot
     *
     * @return \Keios\Monitor\Models\Website[]
     */
    public function getAllUserWebsites($userId, $recognizeRoot = true)
    {
        return $this->getOwnedWebsites($userId, $recognizeRoot, false);
    }


    /**
     * @param int  $userId
     *
     * @param bool $recognizeRoot
     *
     * @return \Keios\Monitor\Models\Website[]
     */
    public function getEnabledUserWebsites($userId, $recognizeRoot = true)
    {

        return $this->getOwnedWebsites($userId, $recognizeRoot, true);
    }


    /**
     * @param int  $userId
     * @param bool $recognizeRoot
     * @param bool $enabled
     *
     * @return array
     */
    protected function getOwnedWebsites($userId, $recognizeRoot, $enabled = false)
    {
        $backendUser = \BackendAuth::getUser();
        //todo roles
        if ($recognizeRoot && $backendUser && $backendUser->is_superuser) {
            /** @var \stdClass[] $ownedProcesses */
            if ($enabled) {
                $ownedWebsites = $this->getEnabledWebsites();
            } else {
                $ownedWebsites = $this->getAllWebsites();
            }
        } else {

            if ($enabled) {
                $ownedWebsites = Website::
                join('keios_monitor_user_websites', 'id', '=', 'keios_monitor_user_websites.website_id')
                    ->where('user_id', $userId)
                    ->rememberForever('km_websites_of_'.$userId)
                    ->get();
            } else {
                $ownedWebsites = Website::
                join('keios_monitor_user_websites', 'id', '=', 'keios_monitor_user_websites.website_id')
                    ->where('user_id', $userId)
                    ->where('is_enabled', 1)
                    ->rememberForever('km_enabled_web_of_'.$userId)
                    ->get();

            }
        }

        return $ownedWebsites;
    }
}