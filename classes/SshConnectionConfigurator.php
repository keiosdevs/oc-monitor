<?php namespace Keios\Monitor\Classes;

use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\Facades\File;
use Keios\Monitor\Models\Server;
use Keios\Monitor\Models\Settings;

/**
 * Class SshConnectionConfigurator
 *
 * @package Keios\Servermanager\Classes
 */
class SshConnectionConfigurator
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var
     */
    protected $cache;

    /**
     * @var Server[]
     */
    protected $servers;

    /**
     * SshConnectionConfigurator constructor.
     *
     * @param Config $repository
     */
    public function __construct(Config $repository)
    {
        $this->config = $repository;
        $this->config->set('remote.default', '');
        $this->servers = Server::all();
    }


    /**
     * Configure all servers for ssh access
     */
    public function configureAll()
    {
        foreach ($this->servers as $server) {
            $this->configure($server);
        }
    }

    /**
     * @param Server $server
     */
    public function configure(Server $server)
    {
        $settings = Settings::instance();
        $keyPath = $settings->get('ssh_key_path');

        $serverDetails = [
            'host'      => $server->host.':'.$server->ssh_port,
            'username'  => $server->ssh_user,
            'key'       => $keyPath,
            'keyphrase' => null,
            'root'      => '/',
            'timeout'   => 5,
        ];

        if (!is_array($this->config->get('remote.connections'))) {
            $this->config->set('remote.connections', []);
        }

        $this->config->set('remote.connections'.'.'.$server->host, $serverDetails);
    }
}