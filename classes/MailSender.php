<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 7:51 AM
 */

namespace Keios\Monitor\Classes;


use Carbon\Carbon;
use Keios\Monitor\Models\Process;
use Keios\Monitor\Models\Server;
use Keios\Monitor\Models\Settings;
use Keios\Monitor\Models\Website;
use Mail;

/**
 * Class MailSender
 *
 * @package Keios\Monitor\Classes
 */
class MailSender
{
    /**
     * @var string
     */
    protected $email;

    /**
     * MailSender constructor.
     */
    public function __construct()
    {
        $settings = Settings::instance();
        $this->email = $settings->get('email');
    }


    /**
     * @param array  $vars
     * @param string $title
     * @param string $email
     * @param string $type
     *
     * @internal param string $name
     */
    public function sendMail($vars, $title, $email, $type)
    {
        Mail::send(
            'keios.monitor::mail'.$type,
            $vars,
            function ($message) use ($email, $title) {
                $message->to($email, 'Keios Administrator');
                $message->subject($title);
            }
        );
    }

    /**
     * @param Server $server
     */
    public function serverUp(Server $server)
    {
        $vars = [
            'server' => $server->host,
            'status' => 'online',
            'date'   => Carbon::now()->toDateTimeString(),
        ];
        $user = $server->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        $title = $server->host.' is back online!';
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'server');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'server');
        }
    }


    /**
     * @param Server $server
     */
    public function serverDown(Server $server)
    {
        $vars = [
            'server' => $server->host,
            'status' => 'offline',
            'date'   => Carbon::now()->toDateTimeString(),
        ];
        $user = $server->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        $title = $server->host.' is offline!';
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'server');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'server');
        }
    }

    /**
     * @param Website $website
     */
    public function websiteUp(Website $website)
    {
        $vars = [
            'url'    => $website->url,
            'status' => 'online',
            'date'   => Carbon::now()->toDateTimeString(),
        ];
        $title = $website->url.' is back online!';
        $user = $website->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'website');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'website');
        }
    }

    /**
     * @param Website $website
     */
    public function websiteDown(Website $website)
    {
        $vars = [
            'url'    => $website->url,
            'status' => 'offline',
            'date'   => Carbon::now()->toDateTimeString(),
        ];
        $title = $website->url.' is offline!';
        $user = $website->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'website');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'website');
        }
    }

    /**
     * @param Process $process
     */
    public function processUp(Process $process)
    {
        $vars = [
            'process' => $process->process_name,
            'host'    => $process->server->host,
            'status'  => 'online',
            'date'    => Carbon::now()->toDateTimeString(),
        ];
        $title = $process->process_name.' is back online!';
        $user = $process->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'process');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'process');
        }
    }

    /**
     * @param Process $process
     */
    public function processDown(Process $process)
    {
        $vars = [
            'process' => $process->process_name,
            'host'    => $process->server->host,
            'status'  => 'offline',
            'date'    => Carbon::now()->toDateTimeString(),
        ];
        $title = $process->process_name.' is offline!';
        $user = $process->user;
        $wantsMail = false;
        if ($user) {
            $wantsMail = $user->monitorprofile->enable_mailer;
        }
        if ($wantsMail) {
            $this->sendMail($vars, $title, $user->email, 'process');
        }
        if ($this->email && $this->email !== $user->email) {
            $this->sendMail($vars, $title, $this->email, 'process');
        }
    }
}