<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/11/16
 * Time: 4:44 PM
 */

namespace Keios\Monitor\Classes;


use Keios\Monitor\Models\LogEntry;
use Keios\Monitor\Models\Settings;

/**
 * Class Logger
 *
 * @package Keios\Monitor\Classes
 */
class Logger
{

    protected $settings;

    protected $enabled;

    public function __construct()
    {
        $this->settings = Settings::instance();
        $this->enabled = $this->settings->get('enable_logging');
    }

    /**
     * @param integer $type 1 - Process, 2 - Server, 3 - Website
     * @param integer $id
     * @param bool    $wasOnline
     *
     * @return null
     * @throws \ApplicationException
     */
    public function log($type, $id, $wasOnline)
    {
        if (!$this->enabled) {
            return null;
        }
        $entity = new LogEntry();
        $entity->type = $type;
        $entity->was_online = $wasOnline;
        switch ($type) {
            case $type === 1:
                $entity->process_id = $id;
                break;
            case $type === 2;
                $entity->server_id = $id;
                break;
            case $type === 3;
                $entity->website_id = $id;
                break;
            default:
                throw new \ApplicationException('Entity type not recognized');
        }
        $entity->save();
    }
}