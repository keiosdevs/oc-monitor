<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/6/16
 * Time: 1:39 PM
 */

namespace Keios\Monitor\Classes;

use SSH;

/**
 * Class SshRunner
 *
 * @package Pixelpixel\Mgmxsimulator\Classes
 */
class SshRunner
{
    /**
     * @param $host
     * @param $commands
     *
     * @return array
     */
    public function runCommands($host, $commands)
    {
        $result = [];
        SSH::into($host)->run(
            $commands,
            function ($out) use (&$result) {
                $line = array_filter(explode(PHP_EOL, $out));
                $result[] = $line;
            }
        );

        return array_flatten($result);
    }
}