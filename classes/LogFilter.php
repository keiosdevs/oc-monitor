<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/17/16
 * Time: 11:21 AM
 */

namespace Keios\Monitor\Classes;


use Carbon\Carbon;

/**
 * Class LogFilter
 *
 * @package Keios\Monitor\Classes
 */
class LogFilter
{
    /**
     * @var int
     */
    public $perPage;
    /**
     * @var Carbon
     */
    public $from;
    /**
     * @var Carbon
     */
    public $to;
    /**
     * @var string website, process, server
     */
    public $type;
    /**
     * @var string url, host or process
     */
    public $name;

    /**
     * LogFilter constructor.
     */
    public function __construct()
    {
        $now = Carbon::now();
        $this->perPage = 10;
        $this->from = $now->copy()->subWeek();
        $this->to = $now->endOfDay();
        $this->type = 'all';
        $this->name = null;
    }
}