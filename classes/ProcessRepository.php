<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 9:55 AM
 */

namespace Keios\Monitor\Classes;

use Keios\Monitor\Models\Process;

/**
 * Class ProcessRepository
 *
 * @package Keios\Monitor\Classes
 */
class ProcessRepository
{

    /**
     *
     * @return Process[]
     */
    public function getAllProcesses()
    {
        return Process::rememberForever('km_proc')->with('server')->get();
    }

    /**
     *
     * @param int $id
     *
     * @return Process
     */
    public function getProcessById($id)
    {
        return Process::rememberForever('km_proc_'.$id)->where('id', $id)->first();
    }

    /**
     * @param integer $userId
     * @param bool    $recognizeRoot
     *
     * @return mixed
     */
    public function getAllUserProcesses($userId, $recognizeRoot = true)
    {
        return $this->getOwnedProcesses($userId, $recognizeRoot, false);
    }

    /**
     * @param int  $userId
     *
     * @param bool $recognizeRoot
     *
     * @return \Keios\Monitor\Models\Process[]
     * @throws \InvalidArgumentException
     */
    public function getEnabledUserProcesses($userId, $recognizeRoot = true)
    {
        return $this->getOwnedProcesses($userId, $recognizeRoot, true);
    }

    /**
     * @param string $term
     *
     * @return Process[]
     */
    public function getProcessesLike($term)
    {
        return Process::where('process_name', 'LIKE', "%$term%")->with('server')->get();
    }

    /**
     * @return Process[]
     */
    public function getEnabledProcesses()
    {
        return Process::where('is_enabled', 1)->rememberForever('km_en_proc')->get();
    }

    /**
     * @param int  $userId
     * @param int  $recognizeRoot
     * @param bool $enabled
     *
     * @return array
     */
    protected function getOwnedProcesses($userId, $recognizeRoot, $enabled = false)
    {
        $backendUser = \BackendAuth::getUser();
        //todo roles
        if ($recognizeRoot && $backendUser && $backendUser->is_superuser) {
            /** @var \stdClass[] $ownedProcesses */
            if ($enabled) {
                $ownedProcesses = $this->getEnabledProcesses();
            } else {
                $ownedProcesses = $this->getAllProcesses();
            }
        } else {

            if ($enabled) {
                $ownedProcesses = Process::with('server')
                    ->join('keios_monitor_user_processes', 'id', '=', 'keios_monitor_user_processes.process_id')
                    ->where('is_enabled', 1)
                    ->where('user_id', $userId)
                    ->rememberForever('km_en_proc_of_'.$userId)
                    ->get();
            } else {
                $ownedProcesses = Process::with('server')
                    ->join('keios_monitor_user_processes', 'id', '=', 'keios_monitor_user_processes.process_id')
                    ->where('user_id', $userId)
                    ->rememberForever('km_proc_of_'.$userId)
                    ->get();
            }
        }

        return $ownedProcesses;
    }
}