<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 7:36 AM
 */

namespace Keios\Monitor\Classes;


use Keios\Monitor\Models\Server;

/**
 * Class CheckServer
 *
 * @package Keios\Monitor\Classes
 */
class CheckServer
{
    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var ServerRepository
     */
    protected $repo;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CheckServer constructor.
     */
    public function __construct()
    {
        $this->sshRunner = new SshRunner();
        $this->mailer = new MailSender();
        $this->logger = new Logger();
        $this->repo = new ServerRepository();
    }


    /**
     * @param int $hostId
     */
    public function makeHostOffline($hostId)
    {
        $user = \Auth::getUser();
        Server::where('id', $hostId)->update(['is_online' => 0]);

        //cache
        $server = $this->repo->getServerById($hostId);
        $this->clearCache([$server], $user->id);
    }

    /**
     * @param string $hostId
     */
    public function makeHostOnline($hostId)
    {
        $user = \Auth::getUser();
        Server::where('id', $hostId)->update(['is_online' => 1]);

        //cache
        $server = $this->repo->getServerById($hostId);
        $this->clearCache([$server], $user->id);
    }

    /**
     * @param Server[] $servers
     * @param int $userId
     */
    public function clearCache($servers, $userId)
    {
        \Cache::forget('km_enabled_servers');
        \Cache::forget('km_servers');
        \Cache::forget('km_servers_of_'.$userId);
        \Cache::forget('km_en_servers_of_'.$userId);
        foreach ($servers as $server) {
            \Cache::forget('km_server_'.$server->id);
        }
    }

    /**
     * @param Server|null $singleServer
     * @param int         $userId
     *
     * @return array
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     */
    public function check($singleServer = null, $userId = null)
    {
        $commands = [
            'uname',
        ];

        if ($singleServer) {
            $wasOnline = $singleServer->is_online;
            try {
                $result = $this->sshRunner->runCommands($singleServer->host, $commands);
            } catch (\Exception $e) {
                $result = [];
            }
            if (!isset($result[0])) {
                $this->makeHostOffline($singleServer->id);
                if ($wasOnline) {
                    $this->logger->log(3, $singleServer->id, $wasOnline);
                    if ($singleServer->send_email) {
                        $this->mailer->serverDown($singleServer);
                    }
                }

                return $singleServer->host.' is down';
            } else {
                if (!$wasOnline) {
                    $this->makeHostOnline($singleServer->id);
                    $this->logger->log(3, $singleServer->id, $wasOnline);
                    if ($singleServer->send_email) {
                        $this->mailer->serverUp($singleServer);
                    }
                }

                return $singleServer->host.' is up';
            }
        }
        if ($userId) {
            $servers = $this->repo->getEnabledUserServers($userId, true);
        } else {
            $servers = $this->repo->getEnabledServers();
        }
        $output = [];

        foreach ($servers as $server) {
            try {
                $result = $this->sshRunner->runCommands($server->host, $commands);
            } catch (\Exception $e) {
                // todo exception ssh exception only
                $result = [];
            }
            if (isset($result[0])) {
                $output[$server->id] = 'up';
            } else {
                $output[$server->id] = 'down';
            }
        }

        $errors = [];
        $success = [];
        foreach ($output as $hostId => $line) {
            $server = $this->repo->getServerById($hostId);
            $wasOnline = $server->is_online;
            if ($line === 'down') {
                $this->makeHostOffline($server->id);
                if ($wasOnline) {
                    \Log::info('Server '.$server->host.' is down!');
                    $this->logger->log(2, $server->id, $wasOnline);
                    if ($server->send_email) {
                        $this->mailer->serverDown($server);
                    }
                }
                $errors[] = $server->host.' is down';
            } elseif ($line === 'up') {
                $this->makeHostOnline($server->id);
                if (!$wasOnline) {
                    \Log::info('Server '.$server->host.' is up!');
                    $this->logger->log(2, $server->id, $wasOnline);
                    if ($server->send_email) {
                        $this->mailer->serverUp($server);
                    }
                }
                $success[] = $server->host.' is up';
            }
        }

        return [
            'errors'  => $errors,
            'success' => $success,
        ];
    }
}