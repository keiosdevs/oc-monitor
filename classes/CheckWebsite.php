<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 7:36 AM
 */

namespace Keios\Monitor\Classes;


use Keios\Monitor\Models\Website;
use RainLab\User\Models\user;

/**
 * Class CheckWebsite
 *
 * @package Keios\Monitor\Classes
 */
class CheckWebsite
{
    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var WebsiteRepository
     */
    protected $repo;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CheckServer constructor.
     */
    public function __construct()
    {
        $this->sshRunner = new SshRunner();
        $this->mailer = new MailSender();
        $this->repo = new WebsiteRepository();
        $this->logger = new Logger();
    }

    /**
     * @param string $url
     */
    public function makeUrlOffline($url)
    {
        $user = \Auth::getUser();
        Website::where('url', $url)->update(['is_online' => 0]);

        //cache
        $websites = $this->repo->getWebsitesByUrl($url);
        $this->clearCache($websites, $user->id);
    }

    /**
     * @param string $url
     */
    public function makeUrlOnline($url)
    {
        $user = \Auth::getUser();
        Website::where('url', $url)->update(['is_online' => 1]);

        //cache
        $websites = $this->repo->getWebsitesByUrl($url);
        $this->clearCache($websites, $user->id);
    }

    /**
     * @param Website[] $websites
     * @param int $userId
     */
    public function clearCache($websites, $userId)
    {
        \Cache::forget('km_enabled_websites');
        \Cache::forget('km_websites');
        \Cache::forget('km_enabled_web_of_'.$userId);
        \Cache::forget('km_website_of_'.$userId);
        foreach ($websites as $website) {
            \Cache::forget('km_website_'.$website->id);
        }
    }

    /**
     * @param Website|null $singleWebsite
     * @param null         $userId
     *
     * @return string
     * @throws \ApplicationException
     *
     */
    public function check(Website $singleWebsite = null, $userId = null)
    {
        if ($singleWebsite) {
            $wasOnline = $singleWebsite->is_online;
            $result = $this->checkWebsite($singleWebsite);
            if (!$result) {
                $this->makeUrlOffline($singleWebsite->url);
                if ($wasOnline) {
                    $this->logger->log(3, $singleWebsite->id, $wasOnline);
                    if ($singleWebsite->send_email) {
                        $this->mailer->websiteDown($singleWebsite);
                    }
                }

                return $singleWebsite->url.' is down';
            } else {
                if (!$wasOnline) {
                    $this->makeUrlOnline($singleWebsite->url);
                    $this->logger->log(3, $singleWebsite->id, $wasOnline);
                    if ($singleWebsite->send_email) {
                        $this->mailer->websiteUp($singleWebsite);
                    }
                }

                return $singleWebsite->url.' is up';
            }
        }

        if ($userId) {
            $websites = $this->repo->getEnabledUserWebsites($userId, true);
        } else {
            $websites = $this->repo->getEnabledWebsites();
        }
        $output = [];
        $results = $this->checkWebsites($websites);

        foreach ($websites as $website) {
            if (array_key_exists($website->id, $results)) {
                $state = 'down';
                if ($results[$website->id] === true) {
                    $state = 'up';
                }
                $output[$website->id] = $state;
            }
        }
        $errors = [];
        $success = [];

        foreach ($output as $websiteId => $line) {
            $website = $this->repo->getWebsiteById($websiteId);
            $wasOnline = $website->is_online;
            if ($line === 'down') {
                $this->makeUrlOffline($website->url);
                if ($wasOnline) {
                    $this->logger->log(3, $website->id, $wasOnline);
                    \Log::info('Website '.$website->url.' is down!');
                    if ($website->send_email) {
                        $this->mailer->websiteDown($website);
                    }
                }

                $errors[] = $website->url.' is down';
            } elseif ($line === 'up') {
                $this->makeUrlOnline($website->url);
                if (!$wasOnline) {
                    \Log::info('Website '.$website->url.' is up!');
                    $this->logger->log(3, $website->id, $wasOnline);
                    if ($website->send_email) {
                        $this->mailer->websiteUp($website);
                    }
                }

                $success[] = $website->url.' is up';
            }
        }

        return [
            'errors'  => $errors,
            'success' => $success,
        ];
    }

    /**
     * @param Website $website
     *
     * @return bool
     * @throws \ApplicationException
     */
    private function checkWebsite(Website $website)
    {
        $handle = curl_init($website->url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($handle, CURLOPT_TIMEOUT, 10);
        /* Get the HTML or whatever is linked in $url. */
        $response = curl_exec($handle);

        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if (!$httpCode) {
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        }

        curl_close($handle);
        $acceptable = [302, 200, 201];
        if (in_array($httpCode, $acceptable, true)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $websites
     *
     * @return array
     */
    private function checkWebsites($websites)
    {
        $handles = [];
        foreach ($websites as $website) {
            $handles[$website->id] = curl_init($website->url);
            curl_setopt($handles[$website->id], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handles[$website->id], CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handles[$website->id], CURLOPT_URL, $website->url);
            curl_setopt($handles[$website->id], CURLOPT_HEADER, 0);
            curl_setopt($handles[$website->id], CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($handles[$website->id], CURLOPT_TIMEOUT, 10);
        }
        $mh = curl_multi_init();
        foreach ($handles as $key => $handle) {
            curl_multi_add_handle($mh, $handle);
        }
        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) == -1) {
                usleep(100);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);

            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
        /* Check for 404 (file not found). */
        $httpCodes = [];
        foreach ($websites as $website) {
            $code = 0;
            $info = curl_getinfo($handles[$website->id]);
            if (array_key_exists('http_code', $info)) {
                $code = $info['http_code'];
            }
            $httpCodes[$website->id] = $code;
        }

        foreach ($handles as $key => $handle) {
            curl_multi_remove_handle($mh, $handle);
        }


        curl_multi_close($mh);

        $acceptable = [302, 200, 201];
        $result = [];
        foreach ($httpCodes as $key => $code) {
            if (in_array($code, $acceptable, false)) {
                $result[$key] = true;
            } else {
                $result[$key] = false;
            }
        }

        return $result;
    }
}