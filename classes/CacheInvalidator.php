<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/17/16
 * Time: 6:23 PM
 */

namespace Keios\Monitor\Classes;

use RainLab\User\Models\User;


/**
 * Class CacheInvalidator
 *
 * @package Keios\Monitor\Classes
 */
class CacheInvalidator
{
    /**
     * @param null|int  $id
     * @param null|User $user
     */
    public function clearWebsitesCache($id = null, $user = null)
    {
        \Cache::forget('km_enabled_websites');
        \Cache::forget('km_websites');
        if ($user) {
            \Cache::forget('km_enabled_web_of_'.$user->id);
            \Cache::forget('km_websites_of_'.$user->id);
        }
        if ($id) {
            \Cache::forget('km_website_'.$id);
        }
    }

    /**
     * @param null|int  $id
     * @param null|User $user
     */
    public function clearServersCache($id = null, $user = null)
    {
        \Cache::forget('km_enabled_servers');
        \Cache::forget('km_servers');
        if ($user) {
            \Cache::forget('km_en_servers_of_'.$user->id);
            \Cache::forget('km_servers_of_'.$user->id);
        }
        if ($id) {
            \Cache::forget('km_servers_'.$id);
        }
    }

    /**
     * @param null|int  $id
     * @param null|User $user
     */
    public function clearProcessCache($id = null, $user = null)
    {
        \Cache::forget('km_proc');
        \Cache::forget('km_en_proc');
        if ($user) {
            \Cache::forget('km_en_proc_of_'.$user->id);
            \Cache::forget('km_proc_of_'.$user->id);
        }
        if ($id) {
            \Cache::forget('km_proc_'.$id);
        }
    }
}