<?php
/**
 * Created by Keios Solutions
 * User: Jakub Zych
 * Date: 5/29/16
 * Time: 10:49 PM
 */

namespace Keios\Monitor\Classes;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\Queue;
use Illuminate\Database\Connection;
use Keios\Monitor\Contracts\JobIdAssignable;
use Keios\Monitor\Contracts\JobStatus;

/**
 * Class JobManager
 * @package Keios\ResellerPortal\Classes
 */
class JobManager
{

    const JOB_TABLE = 'keios_monitor_jobs';

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var Queue
     */
    private $queue;

    /**
     * JobManager constructor.
     *
     * We pass here QueryBuilder instance, connected to VoipSwitch database in
     * the service class and Queue instance, created in Job class
     *
     * @param Connection $db
     * @param Queue      $queue
     */
    public function __construct(Connection $db, Queue $queue)
    {
        $this->db = $db;
        $this->queue = $queue;
    }

    /**
     * Method responsible for dispatching job to Queue and creating its row in DB
     *
     * @param JobIdAssignable $job
     * @param int             $type
     * @param array           $parameters
     *
     * @return int
     */
    public function dispatch(JobIdAssignable $job, $type, array $parameters = [])
    {
        $user = \Auth::getUser();
        $now = Carbon::now()->toDateTimeString();

        array_key_exists('metadata', $parameters)
            ? $metadata = $parameters['metadata']
            : $metadata = '';
        array_key_exists('count', $parameters)
            ? $count = $parameters['count']
            : $count = 0;

        $jobId = $this->db
            ->table(self::JOB_TABLE)
            ->insertGetId(
                [
                    'status'           => JobStatus::IN_PROGRESS,
                    'type'             => $type,
                    'user_id'          => $user->id,
                    'progress_current' => 0,
                    'progress_max'     => $count,
                    'metadata'         => $metadata,
                    'updated_at'       => $now,
                    'created_at'       => $now,
                ]
            );

        $job->assignJobId($jobId);

        $this->queue->push($job);

        return $jobId;
    }

    /**
     * Used by dispatch method. Sets job progress to 0.
     *
     * @param int $id
     * @param int $totalItems
     *
     * @throws \InvalidArgumentException
     */
    public function startJob($id, $totalItems)
    {
        $now = Carbon::now()->toDateTimeString();
        $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->update(
                [
                    'progress_current' => 0,
                    'progress_max'     => $totalItems,
                    'updated_at'       => $now,
                ]
            );
    }

    /**
     * Used after each job loop. Updates current progress of the job.
     *
     * @param int $id
     * @param int $currentItem
     *
     * @throws \InvalidArgumentException
     */
    public function updateJobState($id, $currentItem)
    {
        $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->update(
                [
                    'progress_current' => $currentItem,
                ]
            );
    }

    /**
     * Used after all the loops of the job. Sets job's status to complete.
     *
     * @param int $id
     *
     * @throws \InvalidArgumentException
     */
    public function completeJob($id)
    {
        $totalItems = $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->first(['progress_max']);

        $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->update(
                [
                    'status'           => JobStatus::COMPLETE,
                    'progress_current' => $totalItems->progress_max,
                ]
            );
    }

    /**
     * Jobs can be canceled by updating is_canceled field.
     *
     * @param int    $id
     * @param string $metadata
     *
     * @throws \InvalidArgumentException
     */
    public function cancelJob($id, $metadata)
    {
        $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->update(
                [
                    'status'   => JobStatus::STOPPED,
                    'metadata' => $metadata,
                ]
            );
    }

    /**
     * Job may want to check this field and cancel as required.
     *
     * @param int $id
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function checkIfCanceled($id)
    {
        $status = $this->db->table(self::JOB_TABLE)
            ->where('id', $id)
            ->select('is_canceled')->first()->is_canceled;

        return $status;
    }

    /**
     * Sets job status to failed
     *
     * @param int    $id
     * @param string $metadata
     *
     * @throws \InvalidArgumentException
     */
    public function failJob($id, $metadata)
    {
        $this->db
            ->table(self::JOB_TABLE)
            ->where('id', $id)
            ->update(
                [
                    'status'   => JobStatus::ERROR,
                    'metadata' => json_encode($metadata),
                ]
            );
    }

}