<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/11/16
 * Time: 4:49 PM
 */

namespace Keios\Monitor\Classes;


use Carbon\Carbon;
use Keios\Monitor\Models\LogEntry;
use October\Rain\Database\Builder;
use RainLab\User\Models\User;

/**
 * Class LogRepository
 *
 * @package Keios\Monitor\Classes
 */
class LogRepository
{

    /**
     * @var ServerRepository
     */
    protected $serverRepo;
    /**
     * @var WebsiteRepository
     */
    protected $websiteRepo;
    /**
     * @var ProcessRepository
     */
    protected $processRepo;

    /**
     * LogRepository constructor.
     */
    public function __construct()
    {
        $this->serverRepo = new ServerRepository();
        $this->websiteRepo = new WebsiteRepository();
        $this->processRepo = new ProcessRepository();
    }

    /**
     * @param Carbon $from
     * @param Carbon $to
     * @param bool   $wasOnline
     *
     * @return mixed
     */
    public function getRecords(Carbon $from, Carbon $to, $wasOnline = false)
    {
        return LogEntry::where('created_at', '>=', $from)->where('created_at', '<=', $to)->where(
            'was_online',
            $wasOnline
        )->get();
    }

    /**
     * @param LogFilter $filters
     *
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function getUserLatestRecords($filters)
    {
        $user = \Auth::getUser();
        $websites = $this->websiteRepo->getAllUserWebsites($user->id);
        $processes = $this->processRepo->getAllUserProcesses($user->id);
        $servers = $this->serverRepo->getAllUserServers($user->id);
        $serversIds = [];
        $websitesIds = [];
        $processesIds = [];
        foreach ($servers as $server) {
            $serversIds[] = $server->id;
        }
        foreach ($processes as $process) {
            $processesIds[] = $process->id;
        }
        foreach ($websites as $website) {
            $websitesIds[] = $website->id;
        }
        $result = LogEntry::orderBy('created_at', 'DESC');
        if ($filters->from) {
            $result = $result->where('created_at', '>=', $filters->from);
        }
        if ($filters->to) {
            $result = $result->where('created_at', '<=', $filters->to);
        }
        if ($filters->type !== 'all') {
            if ($filters->type === 'server') {
                $result = $result->where('type', 2)->whereIn('server_id', $serversIds);
            }
            if ($filters->type === 'website') {
                $result = $result->where('type', 3)->whereIn('website_id', $websitesIds);
            }
            if ($filters->type === 'process') {
                $result = $result->where('type', 1)->whereIn('process_id', $processesIds);
            }
        } else {
            $result = $result->whereIn('type', [1,2,3])->whereIn('server_id', $serversIds)->orWhereIn('website_id', $websitesIds)->orWhereIn('process_id', $processesIds);

        }


/*
        if ($filters->name) {
            if ($filters->type === 'server') {
                $result = $this->filterByServersHost($filters, $result);
            }
            if ($filters->type === 'website') {
                $result = $this->filterByWebsitesUrl($filters, $result);
            }
            if ($filters->type === 'process') {
                $result = $this->filterByProcessesName($filters, $result);
            }

            if ($filters->type === 'all') {
                $result = $this->filterByServersHost($filters, $result);
                $result = $this->filterByWebsitesUrl($filters, $result, true);
                $result = $this->filterByProcessesName($filters, $result, true);
            }
        }
*/
        $result = $result->paginate($filters->perPage);

        return $result;
    }

    /**
     * @param LogFilter $filters
     * @param Builder   $query
     * @param bool      $or
     *
     * @return mixed
     */
    protected function filterByServersHost($filters, $query, $or = false)
    {
        $servers = $this->serverRepo->getServersLike($filters->name);
        $serverIds = [];
        foreach ($servers as $server) {
            $serverIds[] = $server->id;
        }
        if ($or) {
            return $query->orWhereIn('server_id', $serverIds);
        }

        return $query->whereIn('server_id', $serverIds);
    }

    /**
     * @param LogFilter $filters
     * @param Builder   $query
     * @param bool      $or
     *
     * @return mixed
     */
    protected function filterByWebsitesUrl($filters, $query, $or = false)
    {
        $websites = $this->websiteRepo->getWebsitesLike($filters->name);
        $websiteIds = [];
        foreach ($websites as $website) {
            $websitesIds[] = $website->id;
        }
        if ($or) {
            return $query->orWhereIn('website_id', $websiteIds);
        }

        return $query->whereIn('website_id', $websiteIds);
    }

    /**
     * @param LogFilter $filters
     * @param Builder   $query
     * @param bool      $or
     *
     * @return mixed
     */
    protected function filterByProcessesName($filters, $query, $or = false)
    {
        $processes = $this->processRepo->getProcessesLike($filters->name);
        $processIds = [];
        foreach ($processes as $process) {
            $processIds[] = $process->id;
        }

        if ($or) {
            return $query->orWhereIn('process_id', $processIds);
        }

        return $query->whereIn('process_id', $processIds);
    }
}