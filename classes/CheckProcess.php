<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/8/16
 * Time: 7:36 AM
 */

namespace Keios\Monitor\Classes;


use Keios\Monitor\Models\Process;
use RainLab\User\Models\User;

/**
 * Class CheckProcess
 *
 * @package Keios\Monitor\Classes
 */
class CheckProcess
{
    /**
     * @var SshRunner
     */
    protected $sshRunner;

    /**
     * @var ProcessRepository
     */
    protected $repo;

    /**
     * @var ServerRepository
     */
    protected $serverRepo;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * CheckServer constructor.
     */
    public function __construct()
    {
        $this->sshRunner = new SshRunner();
        $this->mailer = new MailSender();
        $this->repo = new ProcessRepository();
        $this->logger = new Logger();
        $this->serverRepo = new ServerRepository();
    }


    /**
     * @param int $processId
     */
    public function makeProcessOffline($processId)
    {
        $userId = null;
        $user = \Auth::getUser();
        if($user){
            $userId = $user->id;
        }
        Process::where('id', $processId)->update(['is_online' => 0]);

        //cache
        $process = $this->repo->getProcessById($processId);
        $this->clearCache([$process], $userId);
    }

    /**
     * @param int $processId
     */
    public function makeProcessOnline($processId)
    {
        $userId = null;
        $user = \Auth::getUser();
        if($user){
            $userId = $user->id;
        }
        Process::where('id', $processId)->update(['is_online' => 1]);
        //cache
        $process = $this->repo->getProcessById($processId);
        $this->clearCache([$process], $userId);
    }

    /**
     * @param Process[] $processes
     * @param int $userId
     */
    public function clearCache($processes, $userId)
    {
        \Cache::forget('km_enabled_proc');
        \Cache::forget('km_proc');
        \Cache::forget('km_proc_of_'.$userId);
        foreach ($processes as $process) {
            \Cache::forget('km_proc_'.$process->id);
        }
    }

    /**
     * @param Process|null $singleProcess
     * @param null         $userId
     *
     * @return array|string
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     *
     */
    public function check($singleProcess = null, $userId = null)
    {
        $commands = [
            'no_process',
        ];
        $errors = [];
        $success = [];
        if ($singleProcess) {
            $server = $singleProcess->server;
            $wasOnline = $singleProcess->is_online;
            $name = $singleProcess->process_name;
            if ($singleProcess->check_using === 'ps') {
                $commands = [
                    "ps aux|grep -v grep|grep $name",
                ];
            } elseif ($singleProcess->check_using === 'netstat') {
                $port = $singleProcess->process_port;
                if(!$port){
                    $port = 1;
                }
                $commands = [
                    "netstat -anp|grep $name|grep $port",
                ];
            }
            try {
                $result = $this->sshRunner->runCommands($server->host, $commands);
              } catch (\Exception $e) {
                $result = [];
            }

            if (!isset($result[0])) {
                $this->makeProcessOffline($singleProcess->id);
                if ($wasOnline) {
                    \Log::info('Process '.$singleProcess->process_name .' is down!');
                    $this->logger->log(3, $singleProcess->id, $wasOnline);
                    if ($singleProcess->send_email) {
                        $this->mailer->processDown($singleProcess);
                    }
                }

                $errors[] = $singleProcess->process_name.' is down';

            } else {
                if (!$wasOnline) {
                    if(!strpos($result[0], 'Usage') !== false) {
                        $this->makeProcessOnline($singleProcess->id);
                        \Log::info('Process '.$singleProcess->process_name.' is up!');
                        $this->logger->log(3, $singleProcess->id, $wasOnline);
                        if ($singleProcess->send_email) {
                            $this->mailer->processUp($singleProcess);
                        }
                    }
                }

                $success[] = $singleProcess->process_name.' is up';
            }
            return [
                'errors'  => $errors,
                'success' => $success,
            ];
        }
        if($userId){
            $processes = $this->repo->getEnabledUserProcesses($userId, true);
        } else {
            $processes = $this->repo->getEnabledProcesses();
        }
        $output = [];
        foreach ($processes as $process) {
            $server = $this->serverRepo->getServerById($process->server_id);
            $name = $process->process_name;
            $port = $process->process_port;
            $type = $process->check_using;
            if ($type === 'ps') {
                $commands = [
                    "ps aux|grep -v grep|grep $name",
                ];
            } elseif ($type === 'netstat') {
                $commands = [
                    "netstat -anp|grep $name|grep $port",
                ];
            }
            try {
                $result = $this->sshRunner->runCommands($server->host, $commands);
            } catch (\Exception $e) {
                $result = [];
            }
            if (isset($result[0])) {
                $output[$process->id.'@'.$process->server->id] = 'up';
            } else {
                $output[$process->id.'@'.$process->server->id] = 'down';
            }
        }

        foreach ($output as $processAtHost => $line) {
            $x = explode('@', $processAtHost);
            $processId = $x[0];
            $hostId = $x[1];
            $process = $this->repo->getProcessById($processId);
            $wasOnline = $process->is_online;
            if ($line === 'down') {
                $this->makeProcessOffline($process->id);
                if ($wasOnline) {
                    \Log::info('Process '.$process->process_name.' at '.$process->server->host.' is down!');
                    $this->logger->log(1, $process->id, $wasOnline);
                    if ($process->send_email) {
                        $this->mailer->processDown($process);
                    }
                }
                $errors[] = $process->process_name.' at '.$hostId.' is down';
            } elseif ($line === 'up') {
                $this->makeProcessOnline($process->id);
                if (!$wasOnline) {
                    \Log::info('Process '.$process->process_name.' at '.$hostId.' is up!');
                    $this->logger->log(1, $process->id, $wasOnline);
                    if($process->send_email){
                        $this->mailer->processUp($process);                        
                    }
                }
                $success[] = $process->process_name.' at '.$hostId.' is up';
            }
        }

        return [
            'errors'  => $errors,
            'success' => $success,
        ];
    }
}